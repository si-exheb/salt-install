# Salt Tools Install Repository #


This repository is used for all Salt products installation, configuration and usage @EXHEB.

## Content (directories): ##

* Master-install  :

    Content:
    
    * config-files directory: configuration files for TEST/PROD environnements
    * the documentation for the Salt-master server installation for TEST/PROD environnements
    * the documentation for deploying unique or grouped VMs (with Maps)

* Minions-install :

    * 1) Folder for scripts in developpement
    * 2) Listing of validated OS for install to EXHEB
    * 3) Specific directories for each validated OS version to provide the script to install Salt-minion and its documentation

* Basic install documentation for all products (done on a CentOS 7 server).

* Readme.md file
    
## Prerequisites on all target servers (master & slaves) :##

- username with sudoing capacity (please verify your access before, :-) ) for Linux servers and Administrator rights for Windows servers
- git tool for Linux servers (not invasive, use pearl dependencies,... please check the impact on the target)
- wget tool for Linux servers (not invasive at all,... no restrictions for install it)

## Infos: ##

* I am working now on:

    - the security part of SALT products
    - the approval for the EPFL Security Team must be organized by SM / Nenad
    - salt-cloud deployement system for PROD environnement, if validated by security / SM / Nenad
    
* Please don't hesitate to comment this work, implement it too, make changes,
or simply give me a feedback. Thanks

Philippe Cotter