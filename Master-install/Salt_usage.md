# USAGE of SALT - GENERAL ACTIONS #

## USAGE ON VCENTER-TEST OF EXHEB (ditex-vct.epfl.ch) ##

### USEFUL COMMANDS ###

#### CMD with salt ####

There are the most useful commands to use with SALT is cmd :

With salt '*' cmd.run, you can launch commands directly on the Minion system prompt

* UPDATE --> if you want to make an update on a group of machines or on all:

```

salt '*' cmd.run 'yum update --skip-broken -y'

```


