# Type of VMs: #
-----------------

## small-centos7: ##

  num_cpus: 1
  memory: 512MB
  hard disk size: 10 GB
  
## medium-centos7: ##

  num_cpus: 2
  memory: 1GB
  hard disk size: 20 GB

## big-centos7: ##

  num_cpus: 2
  memory: 2GB
  hard disk size: 50 GB

## heavy-centos7: ##

  num_cpus: 4
  memory: 4GB
  hard disk size: 100 GB