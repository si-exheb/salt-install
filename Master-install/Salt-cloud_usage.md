# USAGE of SALT-CLOUD - PROVISIONNING VIRTUAL MACHINES #

## USAGE ON VCENTER-TEST OF EXHEB (ditex-vct.epfl.ch) ##

### USEFUL COMMANDS ###

Lauch the following commands to list providers, configurations,... etc... :

* salt-cloud --list-locations: it will list the clusters of the provider
* salt-cloud --list-images: it will list the templates of the provider
* salt-cloud --query: it will list all the existing VMs of the provider

```

salt-cloud --list-locations vmware
salt-cloud --list-images vmware
salt-cloud --query vmware

```

### CONFIGURATION FOR INITIAL DEPLOYEMENT OF A VM ON TEST ENV. ###

Lauch the following commands to deploy a CentOS 7 64b from a template on TEST VCenter:

* Using the VMWare cloud.provider.d vmware.conf config file (in TEST env., pointing to 
ditex-vct.epfl.ch, the vmware-test VCenter). The file should be copied in 
the initial deploy of a Salt-cloud master procedure to /etc/salt/cloud.provider.d/ directory.
!!! ATTENTION !!! Please do not forget to change the password with the Keypass password !!!!

* Using the VMWare cloud.profile.d vmware.profile.conf config file. The file should be copied in
the initial deploy of a Salt-cloud master procedure to /etc/salt/cloud.profile.d/ directory.
!!! ATTENTION !!! Please do not forget to change the root password included in the script with the 
standard root password for CentOS templates, see Keypass !!!!

* Using the VMWare cloud.maps.d test-env-demo.map for deploying grouped pack of VMs (test demo of 5 machines).

If it does not done yet, copy them from the local git repo of salt-install with the following commands:

```

cd /home
sudo mkdir gitlocalrepo
cd gitlocalrepo
git clone https://gitlab.epfl.ch/si-exheb/salt-install.git
cd salt-install
sudo cp ./Master-install/config-files/test/etc/salt/cloud.providers.d/vmware.conf /etc/salt/cloud.providers.d/vmware.conf
sudo cp ./Master-install/config-files/test/etc/salt/cloud.profiles.d/vmware.profile.conf /etc/salt/cloud.profiles.d/vmware.profile.conf
sudo cp ./Master-install/config-files/test/etc/salt/cloud.maps.d/test-enf-demo.map /etc/salt/cloud.maps.d/test-env-demo.map

```

You can have more details about the configuration part of this config file on the following link:

https://docs.saltstack.com/en/latest/topics/cloud/vmware.html


### DEPLOY A SIMPLE VM !!! ###

Lauch the following commandot deploy a VM (CentOS7 64b from template) :

* the Template used is a CentOS7-64bits located in the TEST VCenter system
* The actual version of vmware.profile.conf is set for using it
* The command to use is salt-cloud with the parameters:
    * -p for specify the VM profile in VMWare TEST env. (vmware-centos7)
    * the final name of the VM (in our example: testpco1

Use the command:

```

salt-cloud -p small-centos7 exhebtest2

```

You will see in your console:

```

[root@exhebtest1-0 /]# salt-cloud -p vmware-centos7 testpco1
[INFO    ] salt-cloud starting
[INFO    ] Creating testpco1 from template(templ_CentOS7-64)
[INFO    ] [ testpco1 ] Waiting for apply storage DRS recommendations task to finish [0 s]
[INFO    ] [ testpco1 ] Waiting for apply storage DRS recommendations task to finish [5 s]
[INFO    ] [ testpco1 ] Waiting for apply storage DRS recommendations task to finish [10 s]
[INFO    ] [ testpco1 ] Waiting for apply storage DRS recommendations task to finish [15 s]
[INFO    ] [ testpco1 ] Successfully completed apply storage DRS recommendations task in 19 seconds
[INFO    ] [ testpco1 ] Waiting for VMware tools to be running [0 s]
[INFO    ] [ testpco1 ] Waiting for VMware tools to be running [5 s]
[INFO    ] [ testpco1 ] Waiting for VMware tools to be running [10 s]
[INFO    ] [ testpco1 ] Waiting for VMware tools to be running [15 s]
[INFO    ] [ testpco1 ] Waiting for VMware tools to be running [20 s]
[INFO    ] [ testpco1 ] Waiting for VMware tools to be running [25 s]
[INFO    ] [ testpco1 ] Waiting for VMware tools to be running [30 s]
[INFO    ] [ testpco1 ] Waiting for VMware tools to be running [35 s]
[INFO    ] [ testpco1 ] Waiting for VMware tools to be running [40 s]
[INFO    ] [ testpco1 ] Waiting for VMware tools to be running [45 s]
[INFO    ] [ testpco1 ] Successfully got VMware tools running on the guest in 47 seconds
[INFO    ] [ testpco1 ] Waiting to retrieve IPv4 information [0 s]
[INFO    ] [ testpco1 ] Waiting to retrieve IPv4 information [5 s]
[INFO    ] [ testpco1 ] Waiting to retrieve IPv4 information [10 s]
[INFO    ] [ testpco1 ] Waiting to retrieve IPv4 information [15 s]
[INFO    ] [ testpco1 ] Waiting to retrieve IPv4 information [20 s]
[INFO    ] [ testpco1 ] Waiting to retrieve IPv4 information [25 s]
[INFO    ] [ testpco1 ] Waiting to retrieve IPv4 information [30 s]
[INFO    ] [ testpco1 ] Successfully retrieved IPv4 information in 30 seconds
[INFO    ] [ testpco1 ] IPv4 is: 128.178.50.242

```

And after that, you will see a complete deploy report of th VM with HW status, VMWare tools status,... etc...

You can now see it in the VCenter of TEST env., in the "Inventory of VMs and Templates", in the
directory DIT-MA | VAdmin | Creation .

For the this test, the VM have its definitive IP address set in the config file (128.178.50.242, exhebtest2.epfl.ch).

You can connect to it in ssh with default's root password of templates.

The VM comes up and running, VMware tools installed, Minion installed, and incorporated in the Salt-Master, and finally bootstrapped.

### DESTROY A SIMPLE VM !!! ###

Be careful with this command !!! ;-)

* the command is salt-cloud -d (for "D"elete).
* the option -d must to have the name of the VM to destroy (testpco1, for example).

Use the command:

```

salt-cloud -d exhebtest2

```

The shell ask you if you are sure:

```

[root@exhebtest1-0 /]# salt-cloud -d testpco1
[INFO    ] salt-cloud starting
The following virtual machines are set to be destroyed:
  vcenter-test:
    vmware:
      testpco1

Proceed? [N/y] y

```

After press y (yes) and return, you can see these infos:

```

... proceeding
[INFO    ] Destroying in non-parallel mode.
[INFO    ] Powering Off VM testpco1
[INFO    ] Destroying VM testpco1
vcenter-test:
    ----------
    vmware:
        ----------
        testpco1:
            True
[root@exhebtest1-0 /]#


```


### DEPLOY A MAP (GROUP OF VMs) !!! ###

To deploy a massive or grouped VMs pool, you can use the cloud.map function
of Salt-cloud.

To use the TEST env. demo map, you must to copy it from this repo. 
Do the following commands:

```

cd /home
sudo mkdir gitlocalrepo
cd gitlocalrepo
git clone https://gitlab.epfl.ch/si-exheb/salt-install.git
cd salt-install
sudo cp ./Master-install/config-files/test/etc/salt/cloud.maps.d/test-env-demo.map /etc/salt/cloud.maps.d/test-env-demo.map

```

After that, you can use cloud.map command with this demo map (5 VMs will be deployed from the templ_Centos7 on the TEST VCenter.

* The configuration file(s) of maps are located on Salt-master in the following path:
    /etc/salt/cloud.maps.d/
* The map file for our test is test-env-demo.map
* Template used is a CentOS7-64bits located in the TEST VCenter system
* The actual version of vmware.profile.conf is set for using it. It contains
the VMs config files for all concerned machines (exhebtest2-3-4-5-6.profiles.conf)
* The command to use is salt-cloud with the parameters:
    * -m for specify the Map profile of the concerned VMs in VMWare TEST env. (vmware-centos7)
    * -P for parallel deploy
    * MAP_NAME (in our test, test-env-demo.map)

Use the command:

```

salt-cloud -m /etc/salt/cloud.maps.d/test-env-demo.map -P

```

It will create the group of VMs, named exhebtest2, exhebtest3, exhebtest4, exhebtest5 & exhebtest6.

### DESTROY A MAP OF VMs !!! ###

Be careful with this command !!! ;-)

* the command is salt-cloud -m MapFilePathAndName -d (for "D"elete).
* the option -m must to have the name of the MAP concerned (test-env-demo.map in our example).
* the option -d for deletion

Use the command:

```

salt-cloud -m /etc/salt/cloud.maps.d/test-env-demo.map -d

```

It will display the validation question:

```

[root@exhebtest1-0 cloud.maps.d]# salt-cloud -m /etc/salt/cloud.maps.d/test-env-demo.map -d
[INFO    ] salt-cloud starting
[INFO    ] Applying map from '/etc/salt/cloud.maps.d/test-env-demo.map'.
The following virtual machines are set to be destroyed:
  vcenter-test:
    vmware:
      exhebtest2
      exhebtest3
      exhebtest6
      exhebtest4
      exhebtest5

Proceed? [N/y] y
... proceeding
[INFO    ] Destroying in non-parallel mode.
[INFO    ] Powering Off VM exhebtest3
[INFO    ] Destroying VM exhebtest3
[INFO    ] Powering Off VM exhebtest6
[INFO    ] Destroying VM exhebtest6
[INFO    ] Powering Off VM exhebtest4
[INFO    ] Destroying VM exhebtest4
[INFO    ] Powering Off VM exhebtest5
[INFO    ] Destroying VM exhebtest5
[INFO    ] Powering Off VM exhebtest2
[INFO    ] Destroying VM exhebtest2
vcenter-test:
    ----------
    vmware:
        ----------
        exhebtest2:
            True
        exhebtest3:
            True
        exhebtest4:
            True
        exhebtest5:
            True
        exhebtest6:
            True


```

Now the VMs in the map are destroyed

### ANOTERH USEFUL OPTIONS OF SALT.CLOUD TOOL ###

* the option -a to use for actions (reboot, stop, start,...).
* the option -u update the bootstrap (commands file post deploy tool) with the last version on github.com .
* the option -d for deletion
* the option -y to assume "Yes" to all questions

### THE FUTURE ? ###

Now, you are fully able to deploy unique VMs or Maps of VMs.

Enjoy !

Please don't hesitate to comment or give a feedback.

Thanks

Philippe Cotter