# CONFIGURATION OF SALT-MASTER SERVER  #

## INSTALL OF SALT-MASTER SERVER (W. CLOUD OPTIONS) ##

Lauch the following commands:
```
sudo yum install salt-master
sudo yum install salt-cloud
sudo systemctl start salt-master
sudo systemctl enable salt-master
```

# MASTER CONFIGURATION #

By default the Salt master listens on ports 4505 and 4506 on all interfaces (0.0.0.0). 

To bind Salt to a specific IP, redefine the "interface" directive in the master 
configuration file, typically /etc/salt/master, as follows:

```
sudo vi /etc/salt/master
```

And change the "+ interface" value to the right IP for your Salt Master:

```
- #interface: 0.0.0.0
+ interface: xxx.xxx.xxx.xxx
```

# MASTER KEY FINGERPRINT #

Print the master key fingerprint by running the following command on the Salt master:
```
sudo salt-key -F master
```
It will displays that:

```
Local Keys:
master.pem:  a8:ea:54:85:59:9f:bd:04:f7:b4:6f:1d:3d:1b:1d:03
master.pub:  71:d1:55:b0:78:8a:10:64:8b:50:05:03:0a:c9:5e:e6
```

# WINDOWS SLS FILES FRAMEWORK INSTALL #

The SLS files used to install Windows packages are not distributed by default with Salt. Run the following command
to initialize the repository on your Salt master:

```
sudo salt-run winrepo.update_git_repos
```
It will displays that:

```
[cotter@exhebcfg01 ~]$ sudo salt-run winrepo.update_git_repos
[sudo] password for cotter:
https://github.com/saltstack/salt-winrepo-ng.git:
    /srv/salt/win/repo-ng/salt-winrepo-ng
https://github.com/saltstack/salt-winrepo.git:
    /srv/salt/win/repo/salt-winrepo
```

# INSTALL MINION FOR THE SALT-MASTER ITSELF #

To monitor itself as a server, lauch the install of the Minion on itself and 
set the deamon up:

```
sudo yum install salt-minion
sudo systemctl enable salt-minion
sudo systemctl start salt-minion
```

# MODIFICATIONS OF SALT-MASTER CONFIG #

Edit the Salt-Master Config file and add the following::

```
sudo vi /etc/salt/master
```

Add the Salt-Master IP address (for env. dev --> localhost, for env. TEST --> 128.178.50.241 but for PROD --> 128.178.50.18 (exhebcfg1.epfl.ch) ):

```
interface: 128.178.50.241

file_roots:
	base:
		- /srv/salt
	test:
		- /srv/salt/test/services
		- /srv/salt/test/states
	prod:
		- /srv/salt/prod/services
		- /srv/salt/prod/states
```
Save the Config file.

Puis passer les commandes suivantes pour préparer la structure de Salt-Master locale:

```
cd /srv/
sudo mkdir salt
cd salt
sudo mkdir test
mkdir prod
cd test
sudo mkdir services
sudo mkdir states
cd ..
cd prod
sudo mkdir services
sudo mkdir states
cd ..

```


# INSTALL PIP & PYVMOMI COMPONENTS #

To access and use the VMWare API, you must to install pip and pyvmomi tools on
the Salt-master server.

Launch the following commands on the Salt-master:

```
sudo easy_install pip
pip install pyvmomi
```

# COPY THE CLOUD.PROVIDERS.D CONFIG FILE FOR VMWARE #

To access a VMWare datacenter, you must to set a cloud.providers.d config file for our VMWare
datacenter (TEST environnement --> ditex-vct.epfl.ch)

So... copy the right config file (for the good environnement) from the git local repo to 
the right place on the server

```
cd /home
sudo mkdir gitlocalrepo
cd gitlocalrepo
git clone https://gitlab.epfl.ch/si-exheb/salt-install.git
cd salt-install

sudo cp ./Master-install/config-files/test/etc/salt/cloud.providers.d/vmware.conf /etc/salt/cloud.providers.d/vmware.conf


```

# COPY THE CLOUD.PROFILES.D CONFIG FILE FOR VMWARE #

The profile of VMs must be created in this config file and copied to the Salt-master
before to be available to use it.

```
cd /home
cd gitlocalrepo
cd salt-install
git pull

sudo cp ./Master-install/config-files/test/etc/salt/cloud.profiles.d/vmware.profile.conf /etc/salt/cloud.profiles.d/vmware.profile.conf
```

# RESTART OF THE SALT-MASTER DEAMON #

To be sure that all is ok, please restart the service:


```
sudo systemctl restart salt-server
```

You are now fully able to create simply and quick VMs

Philippe C.