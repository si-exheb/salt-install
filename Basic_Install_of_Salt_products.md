# Basic Install of Salt Products on a CentOS 7 64bits server #

1) Import the SaltStack repository key 
```
sudo rpm --import https://repo.saltstack.com/yum/redhat/7/x86_64/latest/SALTSTACK-GPG-KEY.pub
```

2) Create a new repo file
```
sudo vi /etc/yum.repos.d/saltstack.repo
```
coller la suite de la config:
```
[saltstack-repo]
name=SaltStack repo for RHEL/CentOS $releasever
baseurl=https://repo.saltstack.com/yum/redhat/$releasever/$basearch/latest
enabled=1
gpgcheck=1
gpgkey=https://repo.saltstack.com/yum/redhat/$releasever/$basearch/latest/SALTSTACK-GPG-KEY.pub
```
Save and quit

3) "Clean Cache"
```
sudo yum clean expire-cache
```

4) Yum update....

```
sudo yum update
```

5) Install desired Salt Modules (the salt-minion, salt-master, or other Salt components):


Example:

Typically, for a Minion, install salt-minion

```
sudo yum install salt-minion
```

Or... if you want to install a Salt Master with Salt Cloud options:

```
sudo yum install salt-master salt-cloud
```