# Tested and validated OSes for the Minion install #

You can find the different OS versions scripts in the folder 3-Scripts.

## List: ##

Linux OS

```

CentOS 7				--> go to sub-directory named 3-Scripts/centos-rhel_7
RHEL 7					--> go to sub-directory named 3-Scripts/centos-rhel_7
	
CentOS 6				--> go to sub-directory named 3-Scripts/centos-rhel_6
RHEL 6					--> go to sub-directory named 3-Scripts/centos-rhel_6

CentOS 6.7				--> go to sub-directory named 3-Scripts/centos-rhel_6
RHEL 6.7				--> go to sub-directory named 3-Scripts/centos-rhel_6

CentOS 5				--> go to sub-directory named 3-Scripts/centos_5

SLES 11 - SP4           --> go to sub-directory named 3-Scripts/sles_11
SLES 11 - SP3           --> go to sub-directory named 3-Scripts/sles_11
SLES 12 - SP1           --> go to sub-directory named 3-Scripts/sles_12

```

Windows OS

```

Windows 2008 64b		--> go to sub-directory named 3-Scripts/windows-2k8_64-2k12_R2_64
Windows 2012 64b R2		--> go to sub-directory named 3-Scripts/windows-2k8_64-2k12_R2_64


```

Ask me if there is a problem! Thank you !

Philippe Cotter