#!/bin/sh

usage()
{
	echo "Usage: $0 [-h] [-a] [-r] [-m] [-s] "
	echo "\t-h (help - prints this message)"
        echo "\t-a (all - installs Salt repository, master, and minion)"
        echo "\t-r (repo - installs Salt repository only)"
        echo "\t-m (master- installs Salt master)"
        echo "\t-s (slave - installs Salt minion)"
	exit
}


repo() {
    if [ ! -f /etc/yum.repos.d/saltstack.repo ]; then
        pushd /tmp
    	wget https://repo.saltstack.com/yum/rhel7/SALTSTACK-GPG-KEY.pub
    	sudo rpm --import SALTSTACK-GPG-KEY.pub
    	rm -f SALTSTACK-GPG-KEY.pub
        popd
    	sudo cat <<"END" > /etc/yum.repos.d/saltstack.repo
####################
# Enable SaltStack's package repository
[saltstack-repo]
name=SaltStack repo for RHEL/CentOS 7
baseurl=https://repo.saltstack.com/yum/rhel7
enabled=1
gpgcheck=1
gpgkey=https://repo.saltstack.com/yum/rhel7/SALTSTACK-GPG-KEY.pub
END
    fi
}


master() {
    ###
    ### Setup salt-master
    ###

    sudo yum -y install salt-master

    sudo echo 'default_include: master.d/*.conf' >> /etc/salt/master

    sudo mkdir -p /etc/salt/master.d
    sudo mkdir -p /srv/salt/pillar

    sudo cat <<"END" > /etc/salt/master.d/default.conf
fileserver_backend:
  - roots

file_roots:
  dev:
    - /srv/salt

pillar_roots:
  dev:
    - /srv/salt/pillar
END

    sudo systemctl enable salt-master
    sudo systemctl start salt-master
}

slave() {
    ###
    ### Setup salt-minion
    ###

    sudo yum -y install salt-minion

    sudo mkdir -p /etc/salt/minion.d
    sudo echo "master: localhost" > /etc/salt/minion.d/master.conf
    sudo echo "default_include: minion.d/*.conf" >> /etc/salt/minion

    sudo systemctl enable salt-minion
    sudo systemctl start salt-minion
}


#
# MAIN
#

while getopts "harms" flag; do
        case "${flag}" in
                h) usage ;;
                a) repo='true'; master='true'; slave='true' ;;
                r) repo='true' ;;
                m) master='true' ;;
                s) slave='true' ;;
        esac
done

sudo yum -y install wget git

if [ "$repo" == "true" ] ; then
        repo
fi

if [ "$master" == "true" ] ; then
    repo
    master
fi

if [ "$slave" == "true" ] ; then
    repo
    slave
fi

if [ "$master" == "true" ] && [ "$slave" == "true" ] ; then
    ###
    ### Accept all keys
    ###
    sleep 15                #give the minion a few seconds to register
    sudo salt-key -y -A

    ###
    ### Set dev environment
    ###
    sleep 15
    sudo salt '*' grains.setval environment dev
fi