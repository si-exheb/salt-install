# -------------------------------------------------------------------
# Script used for installation of the Salt solutions
# -------------------------------------------------------------------
#
# Origin: Salt devops environnement deploy scripts
#
# Launched by Nenad Buncic @ epfl.ch - EXHEB
# Modified by Philippe Cotter @ epfl.ch - EXHEB
#
# -------------------------------------------------------------------
#
#!/bin/sh
#
#
# Functions sub-routines
# ----------------------
#
# ....................................................................................................
# Option -h --> Help option. It will give you all the options of the script
# ....................................................................................................
#
usage()
{
    clear
    echo ""
    echo "-----------------------------------------------------------------"
    echo "-----------------------------------------------------------------"
    echo "-----------------------------------------------------------------"
    echo "------- HELP switch used.... You need help :-) ------------------"
	echo "-----------------------------------------------------------------"
    echo "-----------------------------------------------------------------"
    echo "-- Here are the possible options of the setup-salt.sh script: ---"
    echo "-----------------------------------------------------------------"
    echo "-----------------------------------------------------------------"
    echo "-----------------------------------------------------------------"
    echo ""
    echo ""
    echo " Usage: $0 [-h] or [-a] or [-r] or [-m] or [-s]                  "
    echo "        or [-t]                                                  "
    echo ""
    echo " -h --> help option - Prints this message, switchs definition    "
    echo ""
    echo " -r --> repo option - The script will install Salt repository    "
    echo ""
    echo " -a --> all option - Installs Salt repository, master, and minion"
    echo "        on a unique server, with parameter of localhost for      "
    echo "        salt-master and salt-minion. Useful for tests on specific"
    echo "        different OSes, but not in PROD or DEV environnement.    "
    echo ""
    echo " -m --> master option - Installs Salt master on localhost        "
    echo " -s --> slave option - Installs Salt minion on localhost         "
    echo " -t --> test option - It will test the OS, Kernel versions       "
    echo ""
    echo "-----------------------------------------------------------------"
    echo "-----------------------------------------------------------------"
    echo "---------  Please use the desired option, thanks. ---------------"
    echo "-----------------------------------------------------------------"
    echo "-----------------------------------------------------------------"
    echo ""
    exit
}
# ....................................................................................................
#
#
# ....................................................................................................
# Option -t Test the environnement of target server 
# ....................................................................................................
#
testos() 
{
	clear
	echo "-----------------------------------------------------"
	echo "-----------------------------------------------------"
	echo "--------- Launch of test of OS sub-routine ----------"
	echo "-----------------------------------------------------"
	echo "-----------------------------------------------------"
	echo ""
	echo " System version: "
	echo " ----------------------------------------------------"
	echo ""
	cat /etc/system-release
	echo ""
	echo " ----------------------------------------------------"
	echo ""
	echo " Hostname, Kernel version and compilation date......."
	echo ""
	uname -a
	echo ""
	echo " ----------------------------------------------------"
	echo ""
	echo " Builder ............................................"
	echo ""
	cat /proc/version
	echo ""
	echo "-----------------------------------------------------"
	echo "-----------------------------------------------------"
	echo "------------ All informations are there  ------------"
	echo "-----------------------------------------------------"
	echo "-----------------------------------------------------"
	echo "-----------------------------------------------------"
	echo ""
}
# ....................................................................................................
#
#
#
# ....................................................................................................
# Option -r --> Repo's install option. Will install the Salt's Repository (actually for CentOS/RHEL 7 only)
# ....................................................................................................
#
repo()
{
    clear
    echo "-----------------------------------------------------"
    echo "-----------------------------------------------------"
    echo " Install of the Repo source for Salt and the GPG key "
    echo "-----------------------------------------------------"
    echo "-----------------------------------------------------"
    echo ""
    echo ""
    sleep 1
    if [ ! -f /etc/yum.repos.d/saltstack.repo ]; then
        echo "Repo never installed, the install start.........."     
        echo ""
        sleep 1
        pushd /tmp
        echo "Import of saltstack-gpg-key.pub.................."
        case "$osversion" in
        	centos7) wget https://repo.saltstack.com/yum/rhel7/SALTSTACK-GPG-KEY.pub;;
			rhel7) wget https://repo.saltstack.com/yum/rhel7/SALTSTACK-GPG-KEY.pub;;
			centos6) wget https://repo.saltstack.com/yum/rhel6/SALTSTACK-GPG-KEY.pub;;
			rhel6) wget https://repo.saltstack.com/yum/rhel6/SALTSTACK-GPG-KEY.pub;;
		esac 
        sudo rpm --import SALTSTACK-GPG-KEY.pub
        rm -f SALTSTACK-GPG-KEY.pub
        popd
        echo ""
        echo "Import of saltstack-gpg-key.pub done - ok ......."
        sleep 1
        echo "Creation of saltstack.repo entry for yum tool    "
        case "$osversion" in
        	centos7)
				sudo cat <<"END" > /etc/yum.repos.d/saltstack.repo
####################
# Enable SaltStack's package repository
[saltstack-repo]
name=SaltStack repo for RHEL/CentOS 7
baseurl=https://repo.saltstack.com/yum/rhel7
enabled=1
gpgcheck=1
gpgkey=https://repo.saltstack.com/yum/rhel7/SALTSTACK-GPG-KEY.pub
END
				;;
			rhel7)
				sudo cat <<"END" > /etc/yum.repos.d/saltstack.repo
####################
# Enable SaltStack's package repository
[saltstack-repo]
name=SaltStack repo for RHEL/CentOS 7
baseurl=https://repo.saltstack.com/yum/rhel7
enabled=1
gpgcheck=1
gpgkey=https://repo.saltstack.com/yum/rhel7/SALTSTACK-GPG-KEY.pub
END
				;;
			centos6)
				sudo cat <<"END" > /etc/yum.repos.d/saltstack.repo
####################
# Enable SaltStack's package repository
[saltstack-repo]
name=SaltStack repo for RHEL/CentOS 6
baseurl=https://repo.saltstack.com/yum/rhel6
enabled=1
gpgcheck=1
gpgkey=https://repo.saltstack.com/yum/rhel6/SALTSTACK-GPG-KEY.pub
END
				;;
			rhel6)
				sudo cat <<"END" > /etc/yum.repos.d/saltstack.repo
####################
# Enable SaltStack's package repository
[saltstack-repo]
name=SaltStack repo for RHEL/CentOS 6
baseurl=https://repo.saltstack.com/yum/rhel6
enabled=1
gpgcheck=1
gpgkey=https://repo.saltstack.com/yum/rhel6/SALTSTACK-GPG-KEY.pub
END
				;;
		esac
        echo ""
        echo "Install of repo done - ok ......................."
        echo ""
    	echo "-----------------------------------------------------"
    	echo ""
    else
		echo "-----------------------------------------------------"
		echo "------------------ CAUTION !!! ----------------------"
		echo "-----------------------------------------------------"
		echo "------------ Repo already installed !!! -------------"
		echo "------------ Not necessary to install ! -------------"
		echo "-----------------------------------------------------"
		echo "-----------------------------------------------------"
		echo ""
    fi
}
#
#
# ....................................................................................................
#
#
#
# ....................................................................................................
# Option -m (Install the Salt Master module)
# ....................................................................................................
#
master()
{
    clear
   	echo "-----------------------------------------------------"
    echo "-----------------------------------------------------"
    echo "---- Install of the Salt Master option started ------"
    echo "-----------------------------------------------------"
    echo "-----------------------------------------------------"
    echo ""
	sleep 1
	if [ ! -f /etc/salt/master ]; then
    	echo ""
        echo "Salt Master service never installed, install will start ..."
        echo ""
        sudo yum -y install salt-master
    	sudo echo 'default_include: master.d/*.conf' >> /etc/salt/master
    	sudo mkdir -p /etc/salt/master.d
    	sudo mkdir -p /srv/salt/pillar
       	case "$env" in
        	dev)
    			sudo cat <<"END" > /etc/salt/master.d/default.conf
fileserver_backend:
  - roots

file_roots:
  dev:
    - /srv/salt

pillar_roots:
  dev:
    - /srv/salt/pillar
END
				;;
			prod)
    			sudo cat <<"END" > /etc/salt/master.d/default.conf
fileserver_backend:
  - roots

file_roots:
  prod:
    - /srv/salt

pillar_roots:
  prod:
    - /srv/salt/pillar
END
				;;
		esac
    	sudo systemctl enable salt-master
    	sudo systemctl start salt-master
    	echo " Salt Master installed . "
    	echo " Salt Master Service started and enabled. "
    else
		clear
		echo ""
		echo "-----------------------------------------------------"
		echo "------------------ CAUTION !!! ----------------------"
		echo "-----------------------------------------------------"
		echo "--------- Salt Master already installed !!! ---------"
		echo "------------  Not possibe to install !  -------------"
		echo "-----------------------------------------------------"
		echo "-----------------------------------------------------"
        echo "-------- Please un-install it before retry ----------"
        echo "--------  or forget to do that !  Thanks. -----------"
        echo "-----------------------------------------------------"
        echo "-----------------------------------------------------"
        echo ""
    fi
}
# ....................................................................................................
#
#
#
# ....................................................................................................
# Option -s (Install the Salt Minion module, the slave....)
# ....................................................................................................
#
#
slave() 
{
#
    clear
  	echo "-----------------------------------------------------"
    echo "-----------------------------------------------------"
    echo "----- Install of the Salt Minion option started -----"
    echo "-----------------------------------------------------"
    echo "-----------------------------------------------------"
    echo ""
    sleep 1
	if [ ! -f /etc/salt/minion ]; then
    	echo ""
        echo "Salt Master service never installed, install will start ..."
        echo ""
		sudo yum -y install salt-minion
		sudo mkdir -p /etc/salt/minion.d
#
        case "$env" in
        	dev)
#    	    	sudo echo "master: 10.92.1.251" > /etc/salt/minion.d/master.conf
# to change when it's rock-solid tested
#
    	    	sudo echo "master: localhost" > /etc/salt/minion.d/master.conf
				;;
			prod)
#    	    	sudo echo "master: exhebcfg01.epfl.ch" > /etc/salt/minion.d/master.conf
# to change when it's rock-solid tested
#
				sudo echo "master: localhost" > /etc/salt/minion.d/master.conf
				;;
		esac
 #   	
    	sudo echo "default_include: minion.d/*.conf" >> /etc/salt/minion
		sudo systemctl enable salt-minion
    	sudo systemctl start salt-minion
    	echo "Install of the Salt Minion done .... all is ok ! "
    else
		clear
		echo ""
		echo "-----------------------------------------------------"
		echo "------------------ CAUTION !!! ----------------------"
		echo "-----------------------------------------------------"
		echo "--------- Salt Minion already installed !!! ---------"
		echo "------------  Not possibe to install !  -------------"
		echo "-----------------------------------------------------"
		echo "-----------------------------------------------------"
        echo "-------- Please un-install it before retry ----------"
        echo "--------  or forget to do that !  Thanks. -----------"
        echo "-----------------------------------------------------"
        echo "-----------------------------------------------------"
        echo ""
    fi
}
#
#
# ....................................................................................................
#
#
#
# ....................................................................................................
# Option Error !!
# ....................................................................................................
#
switcherror() 
{
#
    clear
  	echo "-----------------------------------------------------"
    echo "-----------------------------------------------------"
    echo "----------------   !!! ERROR !!!   ------------------"
    echo "-----------------------------------------------------"
    echo "-----------------------------------------------------"   
    echo "---------  Invalid option... Please try -h ----------"
    echo "-------switch to select a valid one, Thanks ---------"
    echo "-----------------------------------------------------"
    echo "-----------------------------------------------------"
    echo ""
}
#
#
#
#
# ....................................................................................................
#
# MAIN part of the script
#
# ....................................................................................................
#
#
#
clear
echo "-----------------------------------------------------"
echo "-----------------------------------------------------"
echo " Launch of Saltstack setup script "
echo "-----------------------------------------------------"
echo ""
echo ""
#
# ....................................................................................................
# Loop for transfert of switchs to "flags"
# ....................................................................................................
#
while getopts "hte:r:m:s:v" flag; do
        case "${flag}" in
                h) usage ;;
                t) testos='true' ;;
				e) env='$OPTARG' ;;
                r) repo='true'; osversion='$OPTARG' ;;
                m) master='true'; osversion='$OPTARG' ;;
                s) slave='true'; osversion='$OPTARG' ;;
				v) validation='true' ;;
				\?) errorswitch='true';;
        esac
done
#
# ....................................................................................................
# Launch of sub-routines in function of the flags
# ....................................................................................................
#
sleep 1

if [ "$errorswitch" == "true" ] ; then
	switcherror
fi

if [ "$repo" == "true" ] ; then
	repo
fi

if [ "$master" == "true" ] ; then
    repo
    master
fi

if [ "$slave" == "true" ] ; then
    repo
    slave
fi

if [ "$testos" == "true" ] ; then
    testos
fi

if [ "$validation" == "true" ] ; then
    ###
    ### Accept all keys
    ###
    sleep 10               #give the minion a few seconds to register
    sudo salt-key -y -A

    ###
    ### Set dev environment
    ###
    sleep 5
    sudo salt '*' grains.setval environment dev
fi
#
echo ""
echo "-----------------------------------------------------"
echo " End of setup-salt.sh script"
echo "-----------------------------------------------------"
echo ""
