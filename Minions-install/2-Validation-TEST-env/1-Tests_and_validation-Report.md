# Minions installation tests and validation #

This report is made for the Minion's installation tests and validation
for multiple Operating Systems.

## List of the desired OS to test: ##

* CentOS / RHEL 7
* CentOS / RHEL 6
* CentOS 5
* WINDOWS 2008 64 bits / 2012 R2 64 bits
* SLES 11 / 12

## Protocol of tests and validation for linux servers ##

1. choose a TEST server with a type of OS listed
2. connect to it with your user, if not possible, login with root (Keypass)
3. try the sudoing capacity if logged with your user, otherwise, it's ok
4. intial checks 
	* cat /etc/system-release (or cat /etc/redhat-release for CentOS 5 or cat /etc/issue for SLES OS)
	* top
	* df

	take them all to the report
4. install wget & git

Please install the two tools (explain on each version directory documentation).

Some Distributions are different for this part.
    
5. create a local git repo named /home/username/gitlocalrepo, and git clone the
project: git clone https://gitlab.epfl.ch/si-exheb/salt-install.git

```
cd $home
mkdir gitlocalrepo
cd gitlocalrepo/
sudo git clone https://gitlab.epfl.ch/si-exheb/salt-install.git
cd salt-install
cd Install-scripts

```
6. go to the directory of the good version (ex.: centos-rhel_6)
7. launch the install script (ex.: setup-salt-minion-centos6_rhel6.ch)
8. check on the master is the new installed Minion is visible
9. post-install checks 
	* top
	* df

	take them all to the report

10. add the version tested to the supported list.


## Protocol of tests and validation for Windows servers ##

1. choose a TEST server with a type of Windows OS listed
2. connect to it with RDP with an Admin user
3. take measures:
    - Physical Memory percent taked
    - Used RAM
    - Free RAM
    - CPU average
    - Processes Nb.
    - Network usage

	take them all to the report

4. Install Minion Windows agent
5. Check on the master is the new installed Minion is visible
6. Post-install checks, take measures again:
    - Physical Memory percent taked
    - Used RAM
    - Free RAM
    - CPU average
    - Processes Nb.
    - Network usage

	take them all to the report

7. Add the version tested to the supported list.