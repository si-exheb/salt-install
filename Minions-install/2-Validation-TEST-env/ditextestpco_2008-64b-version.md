# Tested and validated OSes for the Minion install #

## List of machines in TEST environnement: ##

### Windows2008 64 bits ###

Machine name: __ditextestpco.epfl.ch__ (temporary test machine)

#### Initial checks: ####

```

Physical Memory percent taked
39%

Used RAM
1600 MB

Free RAM
1960 MB

CPU average
23%

Processes Nb.
52

Network usage
0%

```

#### Application of install procedure: ####
- done !

#### Post-install checks: ####

```

Physical Memory percent taked
41%

Used RAM
1650 MB

Free RAM
1865 MB

CPU average
27%

Processes Nb.
54

Network usage
0%

```



Remarks:

CPU salt-minion to 1.6 % when accessed by the Master, not anymore, otherwise,
no impact. 

- Ram
- CPU
- Disk space


All is ok.

__Validate !__