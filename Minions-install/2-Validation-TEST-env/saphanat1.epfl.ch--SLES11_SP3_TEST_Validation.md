# Tested and validated OSes for the Minion install #

## List of machines in TEST environnement: ##

### SLES 11 (Suse Linux Entreprise Server - SP3)###

Machine name: __saphanat1.epfl.ch__ (VM test SAP machine)

Intial checks:

```

cotter@saphanat1:~> cat /etc/issue

Welcome to SLES for SAP Applications  11.3 (x86_64) - Kernel \r (\l).


cotter@saphanat1:~> top
top - 09:28:22 up 7 days, 13:30,  3 users,  load average: 1.67, 1.14, 0.91
Tasks: 459 total,   2 running, 457 sleeping,   0 stopped,   0 zombie
Cpu(s):  2.2%us,  0.2%sy,  0.0%ni, 97.6%id,  0.0%wa,  0.0%hi,  0.0%si,  0.0%st
Mem:    516740M total,   511716M used,     5023M free,      208M buffers
Swap:     2053M total,        1M used,     2052M free,    80550M cached

  PID USER      PR  NI  VIRT  RES  SHR S   %CPU %MEM    TIME+  COMMAND
 8217 thbadm    20   0  142g 137g 7.0g S     23 27.2   1472:50 hdbindexserver
 4779 root      20   0 71416  37m 3108 R     22  0.0   0:00.65 sdbrun
 7959 thbadm    20   0 45.4g  35g 611m S      7  7.0   1420:15 hdbnameserver
 8219 thbadm    20   0 35.0g  28g 739m S      7  5.6   1087:46 hdbindexserver
 8498 theadm    20   0 10.4g 8.0g 614m S      5  1.6   1333:09 hdbnameserver
 9231 theadm    20   0  174g 168g  14g S      2 33.4   2797:17 hdbindexserver
 9227 theadm    20   0 41.9g  37g 4.8g S      1  7.5   1270:43 hdbindexserver
 9229 theadm    20   0 22.7g  19g 740m S      1  3.8   1046:54 hdbindexserver
 4652 root      20   0 71052 7252 3196 S      0  0.0   2:23.77 snmpd
 4698 cotter    20   0  9204 1432  840 R      0  0.0   0:00.04 top
 4711 root      20   0  105m  11m 9476 S      0  0.0   6:24.42 tina_daemon
 4956 root      20   0  8912  804  384 S      0  0.0   9:47.44 irqbalance
 5710 sapadm    20   0  293m  96m  46m S      0  0.0  26:45.26 sapstartsrv
 9120 theadm    20   0 3549m 874m 151m S      0  0.2  58:47.72 hdbpreprocessor
14828 theadm    20   0 3432m 519m 148m S      0  0.1  28:45.31 hdbwebdispatche
18712 root      20   0     0    0    0 S      0  0.0   0:06.43 kworker/9:7
    1 root      20   0 10560  824  688 S      0  0.0   0:08.92 init
    2 root      20   0     0    0    0 S      0  0.0   0:00.18 kthreadd
    3 root      20   0     0    0    0 S      0  0.0   0:11.60 ksoftirqd/0
    6 root      RT   0     0    0    0 S      0  0.0   0:04.78 migration/0
    7 root      RT   0     0    0    0 S      0  0.0   0:01.38 watchdog/0
    8 root      RT   0     0    0    0 S      0  0.0   0:03.86 migration/1
   10 root      20   0     0    0    0 S      0  0.0   0:02.91 ksoftirqd/1
   12 root      RT   0     0    0    0 S      0  0.0   0:01.05 watchdog/1
   13 root      RT   0     0    0    0 S      0  0.0   0:04.12 migration/2
   14 root      20   0     0    0    0 S      0  0.0   0:06.14 kworker/2:0
   15 root      20   0     0    0    0 S      0  0.0   0:02.31 ksoftirqd/2
   16 root      RT   0     0    0    0 S      0  0.0   0:01.02 watchdog/2
   17 root      RT   0     0    0    0 S      0  0.0   0:04.13 migration/3
   19 root      20   0     0    0    0 S      0  0.0   0:02.36 ksoftirqd/3
   20 root      RT   0     0    0    0 S      0  0.0   0:01.14 watchdog/3
   21 root      RT   0     0    0    0 S      0  0.0   0:04.10 migration/4
   23 root      20   0     0    0    0 S      0  0.0   0:01.75 ksoftirqd/4
   24 root      RT   0     0    0    0 S      0  0.0   0:01.04 watchdog/4
   25 root      RT   0     0    0    0 S      0  0.0   0:03.75 migration/5
   27 root      20   0     0    0    0 S      0  0.0   0:01.66 ksoftirqd/5
   28 root      RT   0     0    0    0 S      0  0.0   0:01.08 watchdog/5
   29 root      RT   0     0    0    0 S      0  0.0   0:04.08 migration/6
cotter@saphanat1:~>

cotter@saphanat1:~> df
Filesystem        1K-blocks       Used  Available Use% Mounted on
/dev/sda5         254902852   21267444  220687084   9% /
udev              264571028        244  264570784   1% /dev
tmpfs             398458880        648  398458232   1% /dev/shm
/dev/sda2           1036108      63764     919712   7% /boot
/dev/sdb1        3219659776  108848404 3110811372   4% /hana/data
/dev/sdb2        2608424552   34443720 2573980832   2% /hana/shared
/dev/sda3          51612856    8490652   40500408  18% /usr/sap
/dev/sdc1        1755976988 1454660996  301315992  83% /backup
/dev/sdd1        5265487872  410158156 4855329716   8% /hana/data2
saptest1:/sapmnt  309505024   20498432  273277952   7% /sapmnt
/dev/md0          783689668   36545880  747143788   5% /hana/log
cotter@saphanat1:~>


```

Install of wget& git

Already installed - OK !

Install of git:

```

saphanat1:~ # sudo zypper addrepo http://download.opensuse.org/repositories/devel:/tools:/scm/SLE_11_SP3/devel:tools:scm.repo
Adding repository 'Software configuration management (SLE_11_SP3)' [done]
Repository 'Software configuration management (SLE_11_SP3)' successfully added
Enabled: Yes
Autorefresh: No
GPG check: Yes
URI: http://download.opensuse.org/repositories/devel:/tools:/scm/SLE_11_SP3/

saphanat1:~ # sudo zypper addrepo http://download.opensuse.org/repositories/devel:/languages:/perl/SLE_11_SP3/devel:languages:perl.repo
Adding repository 'perl modules (SLE_11_SP3)' [done]
Repository 'perl modules (SLE_11_SP3)' successfully added
Enabled: Yes
Autorefresh: No
GPG check: Yes
URI: http://download.opensuse.org/repositories/devel:/languages:/perl/SLE_11_SP3/

saphanat1:~ # sudo zypper install git-core

New repository or package signing key received:
Key ID: DCCA98DDDCEF338C
Key Name: devel:languages:perl OBS Project <devel:languages:perl@build.opensuse.org>
Key Fingerprint: 36F0AC0BCA9D8AF2871703C5DCCA98DDDCEF338C
Key Created: Thu Dec 18 23:36:03 2014
Key Expires: Sat Feb 25 23:36:03 2017
Repository: perl modules (SLE_11_SP3)

Do you want to reject the key, trust temporarily, or trust always? [r/t/a/? shows all options] (r): a
Building repository 'perl modules (SLE_11_SP3)' cache [done]

New repository or package signing key received:
Key ID: 30A8343A498D5A23
Key Name: devel:tools OBS Project <devel:tools@build.opensuse.org>
Key Fingerprint: 428E4E348405CE7900DB99C230A8343A498D5A23
Key Created: Sun May  3 13:32:58 2015
Key Expires: Tue Jul 11 13:32:58 2017
Repository: Software configuration management (SLE_11_SP3)

Do you want to reject the key, trust temporarily, or trust always? [r/t/a/? shows all options] (r): a
Building repository 'Software configuration management (SLE_11_SP3)' cache [done]
Loading repository data...
Reading installed packages...
Resolving package dependencies...

The following items are locked and will not be changed by any action:
  Available:
  GeoIP check-create-certificate java-1_7_0-ibm libGeoIP1 libev4 libqt4-sql-sqlite lprng nginx-1.0
  pcmciautils release-notes-hae ruby-devel rubygem-actionmailer-3_2 rubygem-actionpack-3_2
  rubygem-activemodel-3_2 rubygem-activerecord-3_2 rubygem-activeresource-3_2 rubygem-activesupport-3_2
  rubygem-arel-3_0 rubygem-bcrypt-ruby rubygem-builder-3_0 rubygem-bundler rubygem-cancan
  rubygem-daemon_controller rubygem-devise rubygem-devise-i18n
  rubygem-devise_unix2_chkpwd_authenticatable rubygem-erubis-2_7 rubygem-fast_gettext rubygem-fastthread
  rubygem-gettext_i18n_rails rubygem-haml rubygem-hike rubygem-i18n-0_6 rubygem-journey-1_0
  rubygem-mail-2_4 rubygem-mime-types rubygem-multi_json rubygem-orm_adapter rubygem-passenger
  rubygem-passenger-nginx rubygem-polkit rubygem-polyglot rubygem-rack-1_4 rubygem-rack-cache-1_2
  rubygem-rack-ssl rubygem-rack-test-0_6 rubygem-rails-3_2 rubygem-rails-i18n rubygem-railties-3_2
  rubygem-rake rubygem-rdoc rubygem-ruby-dbus rubygem-sprockets-2_2 rubygem-sqlite3 rubygem-thor
  rubygem-tilt-1_1 rubygem-treetop rubygem-tzinfo rubygem-warden rubygem-webyast-rake-tasks rubygems
  sendmail susehelp_de ulimit webyast-base webyast-base-branding-default yast2-control-center-qt

The following NEW packages are going to be installed:
  git-core git-gui gitk perl-Error

The following packages are not supported by their vendor:
  git-core git-gui gitk perl-Error

4 new packages to install.
Overall download size: 5.7 MiB. After the operation, additional 27.8 MiB will be used.
Continue? [y/n/? shows all options] (y): y
Retrieving package perl-Error-0.17022-35.4.noarch (1/4), 35.0 KiB (70.0 KiB unpacked)
Retrieving: perl-Error-0.17022-35.4.noarch.rpm [done]
Retrieving package git-core-2.7.2-288.1.x86_64 (2/4), 5.1 MiB (25.8 MiB unpacked)
Retrieving: git-core-2.7.2-288.1.x86_64.rpm [done (8.7 MiB/s)]
Retrieving package gitk-2.7.2-288.1.x86_64 (3/4), 247.0 KiB (730.0 KiB unpacked)
Retrieving: gitk-2.7.2-288.1.x86_64.rpm [done]
Retrieving package git-gui-2.7.2-288.1.x86_64 (4/4), 335.0 KiB (1.3 MiB unpacked)
Retrieving: git-gui-2.7.2-288.1.x86_64.rpm [done]
Installing: perl-Error-0.17022-35.4 [done]
Installing: git-core-2.7.2-288.1 [done]
Installing: gitk-2.7.2-288.1 [done]
Installing: git-gui-2.7.2-288.1 [done]


```

Git install done !



Install of Salt-minion via shell script

Post install checks:

```

saphanat1:~/gitlocalrepo/salt-install/Install-Scripts/sles_11 # cat /etc/issue

Welcome to SLES for SAP Applications  11.3 (x86_64) - Kernel \r (\l).


saphanat1:~/gitlocalrepo/salt-install/Install-Scripts/sles_11 # top
top - 09:38:37 up 7 days, 13:41,  3 users,  load average: 1.01, 0.92, 0.88
Tasks: 462 total,   1 running, 461 sleeping,   0 stopped,   0 zombie
Cpu(s):  3.8%us,  0.1%sy,  0.0%ni, 96.0%id,  0.0%wa,  0.0%hi,  0.0%si,  0.0%st
Mem:    516740M total,   511558M used,     5181M free,      213M buffers
Swap:     2053M total,        1M used,     2052M free,    80697M cached

  PID USER      PR  NI  VIRT  RES  SHR S   %CPU %MEM    TIME+  COMMAND
 9227 theadm    20   0 42.4g  37g 4.8g S    187  7.5   1272:22 hdbindexserver
 7959 thbadm    20   0 45.4g  35g 611m S     28  7.0   1421:34 hdbnameserver
 8498 theadm    20   0 10.4g 8.0g 614m S      7  1.6   1334:21 hdbnameserver
 8219 thbadm    20   0 35.0g  28g 739m S      5  5.6   1089:14 hdbindexserver
 9231 theadm    20   0  173g 168g  14g S      4 33.3   2799:38 hdbindexserver
 8217 thbadm    20   0  142g 137g 7.0g S      3 27.2   1474:27 hdbindexserver
 9229 theadm    20   0 23.0g  19g 740m S      1  3.8   1047:58 hdbindexserver
 8161 thbadm    20   0 7928m 5.3g 151m S      1  1.0  54:04.48 hdbpreprocessor
 9120 theadm    20   0 3549m 877m 151m S      1  0.2  58:49.70 hdbpreprocessor
14828 theadm    20   0 3432m 522m 148m S      1  0.1  28:46.89 hdbwebdispatche
  174 root      20   0     0    0    0 S      0  0.0   0:06.96 kworker/42:0
 5802 thbadm    20   0  170m  21m 6328 S      0  0.0   0:40.12 sapstartsrv
 6715 root      20   0  9204 1444  836 R      0  0.0   0:00.02 top
 9118 theadm    20   0 3225m 582m 149m S      0  0.1  29:59.25 hdbcompileserve
 9215 thbadm    20   0 3377m 556m 148m S      0  0.1  24:25.34 hdbwebdispatche
    1 root      20   0 10560  824  688 S      0  0.0   0:08.92 init
    2 root      20   0     0    0    0 S      0  0.0   0:00.18 kthreadd
    3 root      20   0     0    0    0 S      0  0.0   0:11.61 ksoftirqd/0
    6 root      RT   0     0    0    0 S      0  0.0   0:04.78 migration/0
    7 root      RT   0     0    0    0 S      0  0.0   0:01.38 watchdog/0
    8 root      RT   0     0    0    0 S      0  0.0   0:03.86 migration/1
   10 root      20   0     0    0    0 S      0  0.0   0:02.92 ksoftirqd/1
   12 root      RT   0     0    0    0 S      0  0.0   0:01.06 watchdog/1
   13 root      RT   0     0    0    0 S      0  0.0   0:04.12 migration/2
   14 root      20   0     0    0    0 S      0  0.0   0:06.14 kworker/2:0
   15 root      20   0     0    0    0 S      0  0.0   0:02.31 ksoftirqd/2
   16 root      RT   0     0    0    0 S      0  0.0   0:01.02 watchdog/2
   17 root      RT   0     0    0    0 S      0  0.0   0:04.13 migration/3
   19 root      20   0     0    0    0 S      0  0.0   0:02.36 ksoftirqd/3
   20 root      RT   0     0    0    0 S      0  0.0   0:01.14 watchdog/3
   21 root      RT   0     0    0    0 S      0  0.0   0:04.10 migration/4
   23 root      20   0     0    0    0 S      0  0.0   0:01.75 ksoftirqd/4
   24 root      RT   0     0    0    0 S      0  0.0   0:01.04 watchdog/4
   25 root      RT   0     0    0    0 S      0  0.0   0:03.75 migration/5
   27 root      20   0     0    0    0 S      0  0.0   0:01.66 ksoftirqd/5
   28 root      RT   0     0    0    0 S      0  0.0   0:01.08 watchdog/5
   29 root      RT   0     0    0    0 S      0  0.0   0:04.08 migration/6
   31 root      20   0     0    0    0 S      0  0.0   0:01.56 ksoftirqd/6
   
saphanat1:~/gitlocalrepo/salt-install/Install-Scripts/sles_11 # df
Filesystem        1K-blocks       Used  Available Use% Mounted on
/dev/sda5         254902852   21360984  220593544   9% /
udev              264571028        244  264570784   1% /dev
tmpfs             398458880        660  398458220   1% /dev/shm
/dev/sda2           1036108      63764     919712   7% /boot
/dev/sdb1        3219659776  108848404 3110811372   4% /hana/data
/dev/sdb2        2608424552   34438656 2573985896   2% /hana/shared
/dev/sda3          51612856    8490652   40500408  18% /usr/sap
/dev/sdc1        1755976988 1454717588  301259400  83% /backup
/dev/sdd1        5265487872  410158156 4855329716   8% /hana/data2
saptest1:/sapmnt  309505024   20498432  273277952   7% /sapmnt
/dev/md0          783689668   36545880  747143788   5% /hana/log
saphanat1:~/gitlocalrepo/salt-install/Install-Scripts/sles_11 #


```


Remarks:

CPU salt-minion to 0.5 % when accessed by the Master, not anymore, otherwise,
no impact. 

- Ram
- CPU
- Disk space
- SAP engine ok

All is ok.

__Validate !__