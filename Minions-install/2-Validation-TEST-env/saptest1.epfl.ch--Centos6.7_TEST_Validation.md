# Tested and validated OSes for the Minion install #

## List of machines in TEST environnement: ##

### CentOS 6 (6.7) ###

Machine name: __saptest1__ (SAP TEST Machine of Jean-Philippe)

Intial checks:

```

[cotter@saptest1 ~]$ cat /etc/redhat-release
Red Hat Enterprise Linux Server release 6.7 (Santiago)

[cotter@saptest1 ~]$ top
top - 08:31:51 up 21 days,  1:50,  4 users,  load average: 0.31, 0.16, 0.08
Tasks: 413 total,   1 running, 412 sleeping,   0 stopped,   0 zombie
Cpu(s):  0.5%us,  0.1%sy,  0.0%ni, 87.1%id, 12.3%wa,  0.0%hi,  0.0%si,  0.0%st
Mem:  32765628k total, 32420940k used,   344688k free,     1752k buffers
Swap: 100662268k total,  8256132k used, 92406136k free,  7790948k cached

   PID USER      PR  NI  VIRT  RES  SHR S %CPU %MEM    TIME+  COMMAND
141308 ts4adm    20   0 14.0g 979m 864m S  2.3  3.1   1:35.16 TS4_02_DIA_W12
  9875 gh4adm    20   0 2836m  20m 2332 S  0.3  0.1   1:52.60 sapstartsrv
  9908 daaadm    20   0 4993m 404m 3704 S  0.3  1.3 224:44.70 jstart
 10347 root      20   0 1038m 2536 1020 S  0.3  0.0   8:02.66 dsm_sa_datamgrd
 10963 root      20   0  192m 2368 1636 S  0.3  0.0  48:20.32 tina_daemon
 75309 pb4adm    20   0 10.8g 4.5g  13m S  0.3 14.4  82:21.05 jstart
 87674 pj4adm    20   0 10.7g 3.5g  15m S  0.3 11.3  77:43.63 jstart
108754 cotter    20   0 15296 1492  924 R  0.3  0.0   0:00.04 top
     1 root      20   0 19364  344  132 S  0.0  0.0   0:01.91 init
     2 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kthreadd
     3 root      RT   0     0    0    0 S  0.0  0.0   0:01.24 migration/0
     4 root      20   0     0    0    0 S  0.0  0.0   0:03.55 ksoftirqd/0
     5 root      RT   0     0    0    0 S  0.0  0.0   0:00.00 stopper/0
     6 root      RT   0     0    0    0 S  0.0  0.0   0:01.22 watchdog/0
     7 root      RT   0     0    0    0 S  0.0  0.0   0:01.17 migration/1
     8 root      RT   0     0    0    0 S  0.0  0.0   0:00.00 stopper/1
     9 root      20   0     0    0    0 S  0.0  0.0   0:02.58 ksoftirqd/1
    10 root      RT   0     0    0    0 S  0.0  0.0   0:01.06 watchdog/1
    11 root      RT   0     0    0    0 S  0.0  0.0   0:00.72 migration/2
    12 root      RT   0     0    0    0 S  0.0  0.0   0:00.00 stopper/2
    13 root      20   0     0    0    0 S  0.0  0.0   0:02.03 ksoftirqd/2
    14 root      RT   0     0    0    0 S  0.0  0.0   0:01.04 watchdog/2
    15 root      RT   0     0    0    0 S  0.0  0.0   0:01.07 migration/3
    16 root      RT   0     0    0    0 S  0.0  0.0   0:00.00 stopper/3
    17 root      20   0     0    0    0 S  0.0  0.0   0:02.20 ksoftirqd/3
    18 root      RT   0     0    0    0 S  0.0  0.0   0:01.02 watchdog/3
    19 root      RT   0     0    0    0 S  0.0  0.0   0:00.77 migration/4
    20 root      RT   0     0    0    0 S  0.0  0.0   0:00.00 stopper/4
    21 root      20   0     0    0    0 S  0.0  0.0   0:02.89 ksoftirqd/4
    22 root      RT   0     0    0    0 S  0.0  0.0   0:01.04 watchdog/4
    23 root      RT   0     0    0    0 S  0.0  0.0   0:01.85 migration/5
    24 root      RT   0     0    0    0 S  0.0  0.0   0:00.00 stopper/5
    25 root      20   0     0    0    0 S  0.0  0.0   0:02.01 ksoftirqd/5
    26 root      RT   0     0    0    0 S  0.0  0.0   0:01.02 watchdog/5
    27 root      RT   0     0    0    0 S  0.0  0.0   0:00.68 migration/6
    28 root      RT   0     0    0    0 S  0.0  0.0   0:00.00 stopper/6
    29 root      20   0     0    0    0 S  0.0  0.0   0:01.80 ksoftirqd/6
    30 root      RT   0     0    0    0 S  0.0  0.0   0:01.04 watchdog/6
    31 root      RT   0     0    0    0 S  0.0  0.0   0:08.92 migration/7
    32 root      RT   0     0    0    0 S  0.0  0.0   0:00.00 stopper/7
    33 root      20   0     0    0    0 S  0.0  0.0   0:02.46 ksoftirqd/7
    34 root      RT   0     0    0    0 S  0.0  0.0   0:01.12 watchdog/7
    35 root      20   0     0    0    0 S  0.0  0.0   0:52.88 events/0
    36 root      20   0     0    0    0 S  0.0  0.0   0:34.82 events/1
    37 root      20   0     0    0    0 S  0.0  0.0   0:34.47 events/2
    38 root      20   0     0    0    0 S  0.0  0.0   0:35.38 events/3
    39 root      20   0     0    0    0 S  0.0  0.0   0:34.57 events/4

[cotter@saptest1 ~]$ df
Filesystem     1K-blocks     Used Available Use% Mounted on
/dev/sda3       44217456  2012060  39952596   5% /
tmpfs           16382812        0  16382812   0% /dev/shm
/dev/sda1         388480   120582    247418  33% /boot
/dev/sda5        7929200    25440   7494320   1% /tmp
/dev/sda2       50264772  3792192  43912580   8% /usr
/dev/sdc1      309505024 28106760 265669676  10% /usr/sap
/dev/sdd1      309505024 20498760 273277676   7% /sapmnt
/dev/sde1      618883928 34850928 552588876   6% /backup
[cotter@saptest1 ~]$


```

Install of wget:

```

[cotter@saptest1 ~]$ sudo yum install wget
Loaded plugins: product-id, rhnplugin, security, subscription-manager
This system is receiving updates from RHN Classic or RHN Satellite.
Setting up Install Process
Package wget-1.12-5.el6_6.1.x86_64 already installed and latest version
Nothing to do
[cotter@saptest1 ~]$


```

Already installed - OK !


Install of git:

```

[cotter@saptest1 ~]$ sudo yum install git
Loaded plugins: product-id, rhnplugin, security, subscription-manager
This system is receiving updates from RHN Classic or RHN Satellite.
Setting up Install Process
Package git-1.7.1-3.el6_4.1.x86_64 already installed and latest version
Nothing to do
[cotter@saptest1 ~]$

```

Already installed - OK !



Post install checks:


```

[cotter@saptest1 centos-rhel_6]$ cat /etc/issue
Red Hat Enterprise Linux Server release 6.7 (Santiago)
Kernel \r on an \m

[cotter@saptest1 centos-rhel_6]$ top
top - 09:11:32 up 21 days,  2:30,  4 users,  load average: 0.57, 0.86, 0.63
Tasks: 414 total,   1 running, 413 sleeping,   0 stopped,   0 zombie
Cpu(s):  0.2%us,  0.2%sy,  0.0%ni, 98.5%id,  1.0%wa,  0.0%hi,  0.0%si,  0.0%st
Mem:  32765628k total, 32524628k used,   241000k free,     1220k buffers
Swap: 100662268k total,  8206100k used, 92456168k free,  7836172k cached

   PID USER      PR  NI  VIRT  RES  SHR S %CPU %MEM    TIME+  COMMAND
 76934 bw4adm    20   0 9433m 2.4g 2.3g S  0.7  7.8   1:46.60 BW4_00_DIA_W9
    85 root      20   0     0    0    0 S  0.3  0.0   0:12.29 kblockd/3
  8363 bw4adm    20   0 1225m  41m  11m S  0.3  0.1   1:55.03 sapstartsrv
  9908 daaadm    20   0 4993m 402m 2172 S  0.3  1.3 224:57.67 jstart
 10723 root      20   0 3422m 173m  736 S  0.3  0.5  22:07.38 dsm_om_connsvcd
 10963 root      20   0  192m 1976 1244 S  0.3  0.0  48:24.11 tina_daemon
 87674 pj4adm    20   0 10.7g 3.5g  15m S  0.3 11.3  77:52.76 jstart
109347 cotter    20   0 15296 1160  588 R  0.3  0.0   0:00.10 top
     1 root      20   0 19364  344  132 S  0.0  0.0   0:01.92 init
     2 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kthreadd
     3 root      RT   0     0    0    0 S  0.0  0.0   0:01.24 migration/0
     4 root      20   0     0    0    0 S  0.0  0.0   0:03.55 ksoftirqd/0
     5 root      RT   0     0    0    0 S  0.0  0.0   0:00.00 stopper/0
     6 root      RT   0     0    0    0 S  0.0  0.0   0:01.22 watchdog/0
     7 root      RT   0     0    0    0 S  0.0  0.0   0:01.17 migration/1
     8 root      RT   0     0    0    0 S  0.0  0.0   0:00.00 stopper/1
     9 root      20   0     0    0    0 S  0.0  0.0   0:02.59 ksoftirqd/1
    10 root      RT   0     0    0    0 S  0.0  0.0   0:01.07 watchdog/1
    11 root      RT   0     0    0    0 S  0.0  0.0   0:00.72 migration/2
    12 root      RT   0     0    0    0 S  0.0  0.0   0:00.00 stopper/2
    13 root      20   0     0    0    0 S  0.0  0.0   0:02.04 ksoftirqd/2
    14 root      RT   0     0    0    0 S  0.0  0.0   0:01.04 watchdog/2
    15 root      RT   0     0    0    0 S  0.0  0.0   0:01.09 migration/3
    16 root      RT   0     0    0    0 S  0.0  0.0   0:00.00 stopper/3
    17 root      20   0     0    0    0 S  0.0  0.0   0:02.21 ksoftirqd/3
    18 root      RT   0     0    0    0 S  0.0  0.0   0:01.02 watchdog/3
    19 root      RT   0     0    0    0 S  0.0  0.0   0:00.77 migration/4
    20 root      RT   0     0    0    0 S  0.0  0.0   0:00.00 stopper/4
    21 root      20   0     0    0    0 S  0.0  0.0   0:02.89 ksoftirqd/4
    22 root      RT   0     0    0    0 S  0.0  0.0   0:01.04 watchdog/4
    23 root      RT   0     0    0    0 S  0.0  0.0   0:01.85 migration/5
    24 root      RT   0     0    0    0 S  0.0  0.0   0:00.00 stopper/5
    25 root      20   0     0    0    0 S  0.0  0.0   0:02.01 ksoftirqd/5
    26 root      RT   0     0    0    0 S  0.0  0.0   0:01.02 watchdog/5
    27 root      RT   0     0    0    0 S  0.0  0.0   0:00.68 migration/6
    28 root      RT   0     0    0    0 S  0.0  0.0   0:00.00 stopper/6
    29 root      20   0     0    0    0 S  0.0  0.0   0:01.81 ksoftirqd/6
    30 root      RT   0     0    0    0 S  0.0  0.0   0:01.04 watchdog/6
    31 root      RT   0     0    0    0 S  0.0  0.0   0:09.02 migration/7
    32 root      RT   0     0    0    0 S  0.0  0.0   0:00.00 stopper/7
    33 root      20   0     0    0    0 S  0.0  0.0   0:02.46 ksoftirqd/7
    34 root      RT   0     0    0    0 S  0.0  0.0   0:01.12 watchdog/7
    35 root      20   0     0    0    0 S  0.0  0.0   0:52.95 events/0
    36 root      20   0     0    0    0 S  0.0  0.0   0:34.87 events/1
    37 root      20   0     0    0    0 S  0.0  0.0   0:34.51 events/2
    38 root      20   0     0    0    0 S  0.0  0.0   0:35.42 events/3
    39 root      20   0     0    0    0 S  0.0  0.0   0:34.62 events/4
    
[cotter@saptest1 centos-rhel_6]$ df
Filesystem     1K-blocks     Used Available Use% Mounted on
/dev/sda3       44217456  2013424  39951232   5% /
tmpfs           16382812       12  16382800   1% /dev/shm
/dev/sda1         388480   120582    247418  33% /boot
/dev/sda5        7929200    25440   7494320   1% /tmp
/dev/sda2       50264772  3844396  43860376   9% /usr
/dev/sdc1      309505024 28106964 265669472  10% /usr/sap
/dev/sdd1      309505024 20498812 273277624   7% /sapmnt
/dev/sde1      618883928 34850928 552588876   6% /backup
[cotter@saptest1 centos-rhel_6]$


```


Remarks:

CPU salt-minion to 0.7 % when accessed by the Master, not anymore, otherwise,
no impact. 

- Ram
- CPU
- Disk space
- SAP services check

All is ok.

__Validate !__