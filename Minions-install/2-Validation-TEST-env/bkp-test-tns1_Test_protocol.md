# Tested and validated OSes for the Minion install #

## List of machines in TEST environnement: ##

### CentOS 6 (6.7) ###

Machine name: __bkp-test-tns1__ (1st Tina Backup test machine)

Intial checks:

```

*TEST*[cotter@bkp-test-tns1 /]$ cat /etc/system-release
Red Hat Enterprise Linux Server release 6.7 (Santiago)
*TEST*[cotter@bkp-test-tns1 /]$ top

top - 10:15:22 up  1:07,  1 user,  load average: 0.67, 0.26, 0.14
Tasks: 128 total,   2 running, 125 sleeping,   0 stopped,   1 zombie
Cpu(s): 23.7%us,  9.8%sy,  0.0%ni, 56.9%id,  8.1%wa,  0.0%hi,  1.4%si,  0.0%st
Mem:   1922256k total,  1524040k used,   398216k free,   238964k buffers
Swap:  4128764k total,        0k used,  4128764k free,   623288k cached

  PID USER      PR  NI  VIRT  RES  SHR S %CPU %MEM    TIME+  COMMAND                                                                                                                          
 1685 root      20   0     0    0    0 S  3.0  0.0   0:00.30 rpciod/0                                                                                                                         
10296 root      20   0  190m 8920 7264 R  1.7  0.5   0:00.05 .tina_daemon.re                                                                                                                  
 1771 root      20   0     0    0    0 S  1.0  0.0   0:00.12 nfsiod                                                                                                                           
 2449 root      20   0  259m  10m 8660 S  0.3  0.6   0:11.25 .tina_daemon.re                                                                                                                  
10256 cotter    20   0 17124 1236  920 R  0.3  0.1   0:00.01 top                                                                                                                              
    1 root      20   0 21448 1532 1216 S  0.0  0.1   0:01.52 init                                                                                                                             
    2 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kthreadd                                                                                                                         
    3 root      RT   0     0    0    0 S  0.0  0.0   0:00.00 migration/0                                                                                                                      
    4 root      20   0     0    0    0 S  0.0  0.0   0:00.05 ksoftirqd/0                                                                                                                      
    5 root      RT   0     0    0    0 S  0.0  0.0   0:00.00 stopper/0                                                                                                                        
    6 root      RT   0     0    0    0 S  0.0  0.0   0:00.00 watchdog/0                                                                                                                       
    7 root      20   0     0    0    0 S  0.0  0.0   0:00.57 events/0                                                                                                                         
    8 root      20   0     0    0    0 S  0.0  0.0   0:00.00 events/0                                                                                                                         
    9 root      20   0     0    0    0 S  0.0  0.0   0:00.00 events_long/0                                                                                                                    
   10 root      20   0     0    0    0 S  0.0  0.0   0:00.00 events_power_ef                                                                                                                  
   11 root      20   0     0    0    0 S  0.0  0.0   0:00.00 cgroup                                                                                                                           
   12 root      20   0     0    0    0 S  0.0  0.0   0:00.05 khelper                                                                                                                          
   13 root      20   0     0    0    0 S  0.0  0.0   0:00.00 netns                                                                                                                            
   14 root      20   0     0    0    0 S  0.0  0.0   0:00.00 async/mgr                                                                                                                        
   15 root      20   0     0    0    0 S  0.0  0.0   0:00.00 pm                                                                                                                               
   16 root      20   0     0    0    0 S  0.0  0.0   0:00.01 sync_supers                                                                                                                      
   17 root      20   0     0    0    0 S  0.0  0.0   0:00.01 bdi-default                                                                                                                      
   18 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kintegrityd/0                                                                                                                    
   19 root      20   0     0    0    0 S  0.0  0.0   0:00.55 kblockd/0                                                                                                                        
   20 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kacpid                                                                                                                           
   21 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kacpi_notify                                                                                                                     
   22 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kacpi_hotplug                                                                                                                    
   23 root      20   0     0    0    0 S  0.0  0.0   0:00.00 ata_aux                                                                                                                          
   24 root      20   0     0    0    0 S  0.0  0.0   0:00.00 ata_sff/0                                                                                                                        
   25 root      20   0     0    0    0 S  0.0  0.0   0:00.00 ksuspend_usbd                                                                                                                    
   26 root      20   0     0    0    0 S  0.0  0.0   0:00.00 khubd                                                                                                                            
   27 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kseriod                                                                                                                          
   28 root      20   0     0    0    0 S  0.0  0.0   0:00.00 md/0                                                                                                                             
   29 root      20   0     0    0    0 S  0.0  0.0   0:00.00 md_misc/0                                                                                                                        
   30 root      20   0     0    0    0 S  0.0  0.0   0:00.00 linkwatch                                                                                                                        
   31 root      20   0     0    0    0 S  0.0  0.0   0:00.00 khungtaskd                                                                                                                       
   32 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kswapd0                                                                                                                          
   33 root      25   5     0    0    0 S  0.0  0.0   0:00.00 ksmd                                                                                                                             
   34 root      39  19     0    0    0 S  0.0  0.0   0:00.07 khugepaged                                                                                                                       
   35 root      20   0     0    0    0 S  0.0  0.0   0:00.00 aio/0                                                                                                                            
   36 root      20   0     0    0    0 S  0.0  0.0   0:00.00 crypto/0                                                                                                                         
   43 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kthrotld/0                                                                                                                       
   44 root      20   0     0    0    0 S  0.0  0.0   0:00.00 pciehpd                                                                                                                          
   46 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kpsmoused                                                                                                                        
   47 root      20   0     0    0    0 S  0.0  0.0   0:00.00 usbhid_resumer                                                                                                                   
   48 root      20   0     0    0    0 S  0.0  0.0   0:00.00 deferwq                                                                                                                          
   81 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kdmremove                                                                                                                        
   82 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kstriped                                                                                                                         
  185 root      20   0     0    0    0 S  0.0  0.0   0:00.00 scsi_eh_0                                                                                                                        
  186 root      20   0     0    0    0 S  0.0  0.0   0:00.01 scsi_eh_1                                                                                                                        
  192 root      20   0     0    0    0 S  0.0  0.0   0:00.08 mpt_poll_0                                                                                                                       
*TEST*[cotter@bkp-test-tns1 /]$ df
Filesystem           1K-blocks     Used Available Use% Mounted on
/dev/mapper/vg_bkptesttns1-lv_root
                      32452488 19506108  11291188  64% /
tmpfs                   961128        0    961128   0% /dev/shm
/dev/sda1               487652   218585    243467  48% /boot
nas-app-ma-nfs2:/p_bkp_stu1_app
                     105696512 76038464  29658048  72% /tina/vls/1
*TEST*[cotter@bkp-test-tns1 /]$

```

Install of wget:

```

*TEST*[cotter@bkp-test-tns1 /]$ sudo yum install wget
[sudo] password for cotter: 
Sorry, try again.
[sudo] password for cotter: 
Loaded plugins: product-id, refresh-packagekit, security, subscription-manager
Setting up Install Process
Package wget-1.12-5.el6_6.1.x86_64 already installed and latest version
Nothing to do
*TEST*[cotter@bkp-test-tns1 /]$

```

Already installed - OK !


Install of git:

```

*TEST*[cotter@bkp-test-tns1 /]$ sudo yum install git
Loaded plugins: product-id, refresh-packagekit, security, subscription-manager
Setting up Install Process
Package git-1.7.1-3.el6_4.1.x86_64 already installed and latest version
Nothing to do
*TEST*[cotter@bkp-test-tns1 /]$ 

```

Already installed - OK !



Post install checks:


```

Tasks: 128 total,   1 running, 127 sleeping,   0 stopped,   0 zombie
Cpu(s):  0.4%us,  0.0%sy,  0.0%ni, 99.6%id,  0.0%wa,  0.0%hi,  0.0%si,  0.0%st
Mem:   1922256k total,  1731276k used,   190980k free,   242612k buffers
Swap:  4128764k total,        0k used,  4128764k free,   769992k cached

  PID USER      PR  NI  VIRT  RES  SHR S %CPU %MEM    TIME+  COMMAND                          
  192 root      20   0     0    0    0 S  0.4  0.0   0:00.20 mpt_poll_0                       
 2449 root      20   0  259m  10m 8664 S  0.4  0.6   0:27.32 .tina_daemon.re                  
21025 cotter    20   0 17124 1248  928 R  0.4  0.1   0:00.15 top                              
    1 root      20   0 21448 1536 1216 S  0.0  0.1   0:01.59 init                             
    2 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kthreadd                         
    3 root      RT   0     0    0    0 S  0.0  0.0   0:00.00 migration/0                      
    4 root      20   0     0    0    0 S  0.0  0.0   0:00.10 ksoftirqd/0                      
    5 root      RT   0     0    0    0 S  0.0  0.0   0:00.00 stopper/0                        
    6 root      RT   0     0    0    0 S  0.0  0.0   0:00.01 watchdog/0                       
    7 root      20   0     0    0    0 S  0.0  0.0   0:01.03 events/0                         
    8 root      20   0     0    0    0 S  0.0  0.0   0:00.00 events/0                         
    9 root      20   0     0    0    0 S  0.0  0.0   0:00.00 events_long/0                    
   10 root      20   0     0    0    0 S  0.0  0.0   0:00.00 events_power_ef                  
   11 root      20   0     0    0    0 S  0.0  0.0   0:00.00 cgroup                           
   12 root      20   0     0    0    0 S  0.0  0.0   0:00.11 khelper                          
   13 root      20   0     0    0    0 S  0.0  0.0   0:00.00 netns                            
   14 root      20   0     0    0    0 S  0.0  0.0   0:00.00 async/mgr                        
   15 root      20   0     0    0    0 S  0.0  0.0   0:00.00 pm                               
   16 root      20   0     0    0    0 S  0.0  0.0   0:00.03 sync_supers                      
   17 root      20   0     0    0    0 S  0.0  0.0   0:00.03 bdi-default                      
   18 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kintegrityd/0                    
   19 root      20   0     0    0    0 S  0.0  0.0   0:00.64 kblockd/0                        
   20 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kacpid                           
   21 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kacpi_notify                     
   22 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kacpi_hotplug                    
   23 root      20   0     0    0    0 S  0.0  0.0   0:00.00 ata_aux                          
   24 root      20   0     0    0    0 S  0.0  0.0   0:00.00 ata_sff/0                        
   25 root      20   0     0    0    0 S  0.0  0.0   0:00.00 ksuspend_usbd                    
   26 root      20   0     0    0    0 S  0.0  0.0   0:00.00 khubd                            
   27 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kseriod                          
   28 root      20   0     0    0    0 S  0.0  0.0   0:00.00 md/0                             
   29 root      20   0     0    0    0 S  0.0  0.0   0:00.00 md_misc/0                        
   30 root      20   0     0    0    0 S  0.0  0.0   0:00.00 linkwatch                        
   31 root      20   0     0    0    0 S  0.0  0.0   0:00.00 khungtaskd                       
   32 root      20   0     0    0    0 S  0.0  0.0   0:00.06 kswapd0                          
   33 root      25   5     0    0    0 S  0.0  0.0   0:00.00 ksmd                             
   34 root      39  19     0    0    0 S  0.0  0.0   0:00.11 khugepaged                       
   35 root      20   0     0    0    0 S  0.0  0.0   0:00.00 aio/0                            
   36 root      20   0     0    0    0 S  0.0  0.0   0:00.00 crypto/0                         
   43 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kthrotld/0                       
   44 root      20   0     0    0    0 S  0.0  0.0   0:00.00 pciehpd                          
   46 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kpsmoused                        
   47 root      20   0     0    0    0 S  0.0  0.0   0:00.00 usbhid_resumer                   
   48 root      20   0     0    0    0 S  0.0  0.0   0:00.00 deferwq                          
   81 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kdmremove                        
   82 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kstriped                         
  185 root      20   0     0    0    0 S  0.0  0.0   0:00.00 scsi_eh_0                        
  186 root      20   0     0    0    0 S  0.0  0.0   0:00.01 scsi_eh_1                        
  193 root      20   0     0    0    0 S  0.0  0.0   0:00.00 mpt/0                            
  194 root      20   0     0    0    0 S  0.0  0.0   0:00.00 scsi_eh_2                        
  295 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kdmflush                         
*TEST*[cotter@bkp-test-tns1 centos-rhel_6]$ 

*TEST*[cotter@bkp-test-tns1 centos-rhel_6]$ df 
Filesystem           1K-blocks      Used Available Use% Mounted on
/dev/mapper/VolGroup-lv_root
                      35485840   7636928  26039924  23% /
tmpfs                   957296        16    957280   1% /dev/shm
/dev/sda1               487652    104000    358052  23% /boot
nas-app-ma-nfs2:/si_bkp_test_vtl_app
                     524707456 164076992 360630464  32% /opt/mhvtl
*TEST*[cotter@bkp-test-tns1 centos-rhel_6]$ 

```


Remarks:

CPU salt-minion to 1.6 % when accessed by the Master, not anymore, otherwise,
no impact. 

- Ram
- CPU
- Disk space
- Tina services check

All is ok.

__Validate !__

