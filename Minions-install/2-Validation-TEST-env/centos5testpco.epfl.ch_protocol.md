# Tested and validated OSes for the Minion install #

## List of machines in TEST environnement: ##

### CentOS 5 ###

Machine name: __centos5testpco.epfl.ch__ (VM on my laptop)

Intial checks:

```

[cotter@centos5testpco gitlocalrepo]$  cat /etc/redhat-release
CentOS release 5.11 (Final)
[cotter@centos5testpco gitlocalrepo]$ 

[cotter@centos5testpco gitlocalrepo]$ top
top - 05:45:18 up 23 min,  2 users,  load average: 0.02, 0.07, 0.07
Tasks: 160 total,   1 running, 159 sleeping,   0 stopped,   0 zombie
Cpu(s):  2.3%us,  0.9%sy,  0.0%ni, 96.5%id,  0.0%wa,  0.3%hi,  0.0%si,  0.0%st
Mem:   1023864k total,   792896k used,   230968k free,    76556k buffers
Swap:  2064344k total,      160k used,  2064184k free,   410348k cached

  PID USER      PR  NI  VIRT  RES  SHR S %CPU %MEM    TIME+  COMMAND                                                                 
 5173 cotter    16   0  285m  19m  10m S  8.5  1.9   0:02.99 gnome-terminal                                                          
 4788 root      15   0  150m  22m 6256 S  4.0  2.3   0:06.57 Xorg                                                                    
   10 root      10  -5     0    0    0 S  0.2  0.0   0:00.59 events/0                                                                
 4298 root      16   0 10252  752  644 S  0.2  0.1   0:00.69 hald-addon-stor                                                         
    1 root      15   0 10372  696  588 S  0.0  0.1   0:01.14 init                                                                    
    2 root      RT  -5     0    0    0 S  0.0  0.0   0:00.00 migration/0                                                             
    3 root      34  19     0    0    0 S  0.0  0.0   0:00.00 ksoftirqd/0                                                             
    4 root      RT  -5     0    0    0 S  0.0  0.0   0:00.00 migration/1                                                             
    5 root      34  19     0    0    0 S  0.0  0.0   0:00.00 ksoftirqd/1                                                             
    6 root      RT  -5     0    0    0 S  0.0  0.0   0:00.02 migration/2                                                             
    7 root      34  19     0    0    0 S  0.0  0.0   0:00.30 ksoftirqd/2                                                             
    8 root      RT  -5     0    0    0 S  0.0  0.0   0:00.00 migration/3                                                             
    9 root      34  19     0    0    0 S  0.0  0.0   0:00.00 ksoftirqd/3                                                             
   11 root      10  -5     0    0    0 S  0.0  0.0   0:00.00 events/1                                                                
   12 root      10  -5     0    0    0 S  0.0  0.0   0:00.03 events/2                                                                
   13 root      10  -5     0    0    0 S  0.0  0.0   0:00.03 events/3                                                                
   14 root      11  -5     0    0    0 S  0.0  0.0   0:00.00 khelper                                                                 
   23 root      11  -5     0    0    0 S  0.0  0.0   0:00.00 kthread                                                                 
   30 root      10  -5     0    0    0 S  0.0  0.0   0:00.02 kblockd/0                                                               
   31 root      10  -5     0    0    0 S  0.0  0.0   0:00.00 kblockd/1                                                               
   32 root      10  -5     0    0    0 S  0.0  0.0   0:00.18 kblockd/2                                                               
   33 root      10  -5     0    0    0 S  0.0  0.0   0:00.00 kblockd/3                                                               
   34 root      17  -5     0    0    0 S  0.0  0.0   0:00.00 kacpid                                                                  
  208 root      13  -5     0    0    0 S  0.0  0.0   0:00.00 cqueue/0                                                                
  209 root      14  -5     0    0    0 S  0.0  0.0   0:00.00 cqueue/1                                                                
  210 root      14  -5     0    0    0 S  0.0  0.0   0:00.00 cqueue/2                                                                
  211 root      14  -5     0    0    0 S  0.0  0.0   0:00.00 cqueue/3                                                                
  214 root      10  -5     0    0    0 S  0.0  0.0   0:00.00 khubd                                                                   
  216 root      10  -5     0    0    0 S  0.0  0.0   0:00.00 kseriod                                                                 
  310 root      15   0     0    0    0 S  0.0  0.0   0:00.00 khungtaskd                                                              
  311 root      15   0     0    0    0 S  0.0  0.0   0:00.48 pdflush                                                                 
  312 root      15   0     0    0    0 S  0.0  0.0   0:00.77 pdflush                                                                 
  313 root      10  -5     0    0    0 S  0.0  0.0   0:00.15 kswapd0                                                                 
  314 root      13  -5     0    0    0 S  0.0  0.0   0:00.00 aio/0                                                                   
  315 root      14  -5     0    0    0 S  0.0  0.0   0:00.00 aio/1                                                                   
  316 root      13  -5     0    0    0 S  0.0  0.0   0:00.00 aio/2     
  
[cotter@centos5testpco gitlocalrepo]$ df
Filesystem           1K-blocks      Used Available Use% Mounted on
/dev/sda2             28174460   3333916  23386260  13% /
/dev/sda1               295561     23220    257081   9% /boot
tmpfs                   511932         0    511932   0% /dev/shm
[cotter@centos5testpco gitlocalrepo]$ 


```

Install of wget:

--> Already installed - OK !


Install of git:


--> Already installed - OK !



Post install checks:


```
[root@centos5testpco etc]# top


top - 06:09:20 up 47 min,  2 users,  load average: 0.08, 0.06, 0.03
Tasks: 163 total,   1 running, 162 sleeping,   0 stopped,   0 zombie
Cpu(s):  1.0%us,  0.7%sy,  0.0%ni, 98.1%id,  0.0%wa,  0.2%hi,  0.0%si,  0.0%st
Mem:   1023864k total,   942420k used,    81444k free,    82888k buffers
Swap:  2064344k total,      160k used,  2064184k free,   520336k cached

  PID USER      PR  NI  VIRT  RES  SHR S %CPU %MEM    TIME+  COMMAND                     
 5173 cotter    15   0  285m  19m  10m S  4.0  1.9   0:06.85 gnome-terminal              
 4788 root      15   0  143m  16m 6744 S  2.0  1.6   0:13.37 Xorg                        
 4300 root      16   0 10252  752  644 S  0.3  0.1   0:01.44 hald-addon-stor             
 4919 cotter    15   0  284m  15m  12m S  0.3  1.6   0:00.73 vmtoolsd                    
 7736 root      15   0 12764 1336  956 R  0.3  0.1   0:00.02 top                         
    1 root      15   0 10372  696  588 S  0.0  0.1   0:01.15 init                        
    2 root      RT  -5     0    0    0 S  0.0  0.0   0:00.00 migration/0                 
    3 root      34  19     0    0    0 S  0.0  0.0   0:00.01 ksoftirqd/0                 
    4 root      RT  -5     0    0    0 S  0.0  0.0   0:00.00 migration/1                 
    5 root      34  19     0    0    0 S  0.0  0.0   0:00.00 ksoftirqd/1                 
    6 root      RT  -5     0    0    0 S  0.0  0.0   0:00.02 migration/2                 
    7 root      34  19     0    0    0 S  0.0  0.0   0:00.30 ksoftirqd/2                 
    8 root      RT  -5     0    0    0 S  0.0  0.0   0:00.00 migration/3                 
    9 root      34  19     0    0    0 S  0.0  0.0   0:00.00 ksoftirqd/3                 
   10 root      10  -5     0    0    0 S  0.0  0.0   0:01.26 events/0                    
   11 root      10  -5     0    0    0 S  0.0  0.0   0:00.00 events/1                    
   12 root      10  -5     0    0    0 S  0.0  0.0   0:00.04 events/2                    
   13 root      10  -5     0    0    0 S  0.0  0.0   0:00.16 events/3                    
   14 root      11  -5     0    0    0 S  0.0  0.0   0:00.00 khelper                     
   23 root      11  -5     0    0    0 S  0.0  0.0   0:00.00 kthread                     
   30 root      10  -5     0    0    0 S  0.0  0.0   0:00.02 kblockd/0                   
   31 root      10  -5     0    0    0 S  0.0  0.0   0:00.00 kblockd/1                   
   32 root      10  -5     0    0    0 S  0.0  0.0   0:00.19 kblockd/2                   
   33 root      10  -5     0    0    0 S  0.0  0.0   0:00.00 kblockd/3                   
   34 root      17  -5     0    0    0 S  0.0  0.0   0:00.00 kacpid                      
  208 root      13  -5     0    0    0 S  0.0  0.0   0:00.00 cqueue/0                    
  209 root      14  -5     0    0    0 S  0.0  0.0   0:00.00 cqueue/1                    
  210 root      14  -5     0    0    0 S  0.0  0.0   0:00.00 cqueue/2                    
  211 root      14  -5     0    0    0 S  0.0  0.0   0:00.00 cqueue/3                    
  214 root      10  -5     0    0    0 S  0.0  0.0   0:00.00 khubd                       
  216 root      10  -5     0    0    0 S  0.0  0.0   0:00.00 kseriod                     
  310 root      15   0     0    0    0 S  0.0  0.0   0:00.00 khungtaskd                  
  311 root      15   0     0    0    0 S  0.0  0.0   0:00.48 pdflush                     
  312 root      15   0     0    0    0 S  0.0  0.0   0:01.02 pdflush                     
  313 root      10  -5     0    0    0 S  0.0  0.0   0:00.17 kswapd0                     
  314 root      13  -5     0    0    0 S  0.0  0.0   0:00.00 aio/0                       
[root@centos5testpco etc]# 

```

```
[root@centos5testpco etc]# df
Filesystem           1K-blocks      Used Available Use% Mounted on
/dev/sda2             28174460   3438864  23281312  13% /
/dev/sda1               295561     23220    257081   9% /boot
tmpfs                   511932        12    511920   1% /dev/shm
[root@centos5testpco etc]# 


```


Remarks:

CPU salt-minion to 1.6 % when accessed by the Master, not anymore, otherwise,
no impact. 

- Ram
- CPU
- Disk space

All is ok.

__Validate !__