# Tested and validated OSes for the Minion install #

## List of machines in TEST environnement: ##

### SLES 11 (Suse Linux Entreprise Server - SP4)###

Machine name: __sles11-sp4-testpco.epfl.ch__ (VM test machine)

Intial checks:

```

cotter@linux-rlsj:~/gitlocalrepo/salt-install/Install-Scripts/sles_11> cat /etc/issue

Welcome to SUSE Linux Enterprise Server 11 SP4  (x86_64) - Kernel \r (\l).


cotter@linux-rlsj:~/gitlocalrepo/salt-install/Install-Scripts/sles_11> top

top - 07:05:13 up  1:51,  2 users,  load average: 0.17, 0.23, 0.14
Tasks: 156 total,   2 running, 154 sleeping,   0 stopped,   0 zombie
Cpu(s):  0.4%us,  0.3%sy,  0.0%ni, 99.2%id,  0.1%wa,  0.0%hi,  0.0%si,  0.0%st
Mem:   1014120k total,   876076k used,   138044k free,    47080k buffers
Swap:  1524732k total,      968k used,  1523764k free,   594216k cached

  PID USER      PR  NI  VIRT  RES  SHR S   %CPU %MEM    TIME+  COMMAND                                                                                                                        
   87 root      20   0     0    0    0 S      2  0.0   0:02.67 kworker/2:1                                                                                                                    
    1 root      20   0 10560  764  704 S      0  0.1   0:00.83 init                                                                                                                           
    2 root      20   0     0    0    0 S      0  0.0   0:00.00 kthreadd                                                                                                                       
    3 root      20   0     0    0    0 S      0  0.0   0:00.48 ksoftirqd/0                                                                                                                    
    6 root      RT   0     0    0    0 S      0  0.0   0:00.07 migration/0                                                                                                                    
    7 root      RT   0     0    0    0 S      0  0.0   0:00.01 watchdog/0                                                                                                                     
    8 root      RT   0     0    0    0 S      0  0.0   0:00.08 migration/1                                                                                                                    
   10 root      20   0     0    0    0 S      0  0.0   0:00.06 ksoftirqd/1                                                                                                                    
   12 root      RT   0     0    0    0 S      0  0.0   0:00.01 watchdog/1                                                                                                                     
   13 root      RT   0     0    0    0 S      0  0.0   0:00.21 migration/2                                                                                                                    
   14 root      20   0     0    0    0 S      0  0.0   0:00.00 kworker/2:0                                                                                                                    
   15 root      20   0     0    0    0 S      0  0.0   0:00.02 ksoftirqd/2                                                                                                                    
   16 root      RT   0     0    0    0 S      0  0.0   0:00.01 watchdog/2                                                                                                                     
   17 root      RT   0     0    0    0 S      0  0.0   0:00.07 migration/3                                                                                                                    
   19 root      20   0     0    0    0 S      0  0.0   0:00.02 ksoftirqd/3                                                                                                                    
   20 root      RT   0     0    0    0 S      0  0.0   0:00.01 watchdog/3                                                                                                                     
   21 root       0 -20     0    0    0 S      0  0.0   0:00.00 cpuset                                                                                                                         
   22 root       0 -20     0    0    0 S      0  0.0   0:00.00 khelper                                                                                                                        
   23 root       0 -20     0    0    0 S      0  0.0   0:00.00 netns                                                                                                                          
   24 root      20   0     0    0    0 S      0  0.0   0:00.00 sync_supers                                                                                                                    
   25 root      20   0     0    0    0 S      0  0.0   0:00.00 bdi-default                                                                                                                    
   26 root       0 -20     0    0    0 S      0  0.0   0:00.00 kintegrityd                                                                                                                    
   27 root       0 -20     0    0    0 S      0  0.0   0:00.00 kblockd                                                                                                                        
   28 root       0 -20     0    0    0 S      0  0.0   0:00.00 md                                                                                                                             
   29 root      20   0     0    0    0 S      0  0.0   0:00.00 khungtaskd                                                                                                                     
   30 root      20   0     0    0    0 S      0  0.0   0:00.22 kswapd0                                                                                                                        
   31 root      25   5     0    0    0 S      0  0.0   0:00.00 ksmd                                                                                                                           
   32 root      39  19     0    0    0 S      0  0.0   0:00.05 khugepaged                                                                                                                     
   33 root      20   0     0    0    0 S      0  0.0   0:00.00 fsnotify_mark                                                                                                                  
   34 root       0 -20     0    0    0 S      0  0.0   0:00.00 crypto                                                                                                                         
   38 root       0 -20     0    0    0 S      0  0.0   0:00.00 kthrotld                                                                                                                       
   39 root       0 -20     0    0    0 S      0  0.0   0:00.00 kpsmoused                                                                                                                      
   84 root       0 -20     0    0    0 S      0  0.0   0:00.00 mpt_poll_0                                                                                                                     
   85 root       0 -20     0    0    0 S      0  0.0   0:00.00 mpt/0                                                                                                                          
   86 root      20   0     0    0    0 S      0  0.0   0:00.00 kworker/3:1                                                                                                                    
   88 root      20   0     0    0    0 S      0  0.0   0:00.00 scsi_eh_0                                                                                                                      
   96 root       0 -20     0    0    0 S      0  0.0   0:00.00 ata_sff                                                                                                                        
   97 root      20   0     0    0    0 S      0  0.0   0:00.07 scsi_eh_1                                                                                                                      
   98 root      20   0     0    0    0 S      0  0.0   0:00.08 scsi_eh_2                                                                                                                      
   99 root      20   0     0    0    0 S      0  0.0   0:00.00 scsi_eh_3                                                                                                                      
  100 root      20   0     0    0    0 S      0  0.0   0:00.00 scsi_eh_4                                                                                                                      
  101 root      20   0     0    0    0 S      0  0.0   0:00.00 scsi_eh_5                                                                                                                      
  102 root      20   0     0    0    0 S      0  0.0   0:00.00 scsi_eh_6                                                                                                                      
  103 root      20   0     0    0    0 S      0  0.0   0:00.00 scsi_eh_7                                                                                                                      
  104 root      20   0     0    0    0 S      0  0.0   0:00.00 scsi_eh_8                                                                                                                      
  105 root      20   0     0    0    0 S      0  0.0   0:00.00 scsi_eh_9                                                                                                                      
  106 root      20   0     0    0    0 S      0  0.0   0:00.00 scsi_eh_10                                                                                                                     
  107 root      20   0     0    0    0 S      0  0.0   0:00.00 scsi_eh_11                                                                                                                     
  108 root      20   0     0    0    0 S      0  0.0   0:00.00 scsi_eh_12                                                                                                                     
cotter@linux-rlsj:~/gitlocalrepo/salt-install/Install-Scripts/sles_11>

cotter@linux-rlsj:~/gitlocalrepo/salt-install/Install-Scripts/sles_11> df
Filesystem     1K-blocks    Used Available Use% Mounted on
/dev/sda2       29461480 4047496  23917408  15% /
udev              507060     124    506936   1% /dev
tmpfs             507060      84    506976   1% /dev/shm
/dev/sr0         3304920 3304920         0 100% /media/SLES-11-SP4-DVD-x86_6412211
cotter@linux-rlsj:~/gitlocalrepo/salt-install/Install-Scripts/sles_11>

```

Install of wget& git

Already installed - OK !

Install of git:

Already installed - OK !

Install of Salt-minion via shell script

Post install checks:

```

cotter@linux-rlsj:~/gitlocalrepo/salt-install/Install-Scripts/sles_11> cat /etc/issue

Welcome to SUSE Linux Enterprise Server 11 SP4  (x86_64) - Kernel \r (\l).


cotter@linux-rlsj:~/gitlocalrepo/salt-install/Install-Scripts/sles_11> top

top - 07:16:46 up  2:03,  2 users,  load average: 0.52, 0.32, 0.25
Tasks: 157 total,   1 running, 156 sleeping,   0 stopped,   0 zombie
Cpu(s):  0.4%us,  0.1%sy,  0.0%ni, 99.5%id,  0.0%wa,  0.0%hi,  0.0%si,  0.0%st
Mem:   1014120k total,   934812k used,    79308k free,    47984k buffers
Swap:  1524732k total,     1476k used,  1523256k free,   625924k cached

  PID USER      PR  NI  VIRT  RES  SHR S   %CPU %MEM    TIME+  COMMAND                                                                                                                        
15703 root      20   0  227m  53m   9m S      1  5.4   0:18.45 X                                                                                                                              
16208 cotter    20   0  318m  19m  14m S      0  2.0   0:01.96 main-menu                                                                                                                      
17415 cotter    20   0  219m  14m  10m S      0  1.5   0:05.26 gnome-terminal                                                                                                                 
40156 cotter    20   0  8940 1192  856 R      0  0.1   0:00.23 top                                                                                                                            
    1 root      20   0 10560  764  704 S      0  0.1   0:00.84 init                                                                                                                           
    2 root      20   0     0    0    0 S      0  0.0   0:00.00 kthreadd                                                                                                                       
    3 root      20   0     0    0    0 S      0  0.0   0:01.08 ksoftirqd/0                                                                                                                    
    6 root      RT   0     0    0    0 S      0  0.0   0:00.08 migration/0                                                                                                                    
    7 root      RT   0     0    0    0 S      0  0.0   0:00.01 watchdog/0                                                                                                                     
    8 root      RT   0     0    0    0 S      0  0.0   0:00.08 migration/1                                                                                                                    
   10 root      20   0     0    0    0 S      0  0.0   0:00.06 ksoftirqd/1                                                                                                                    
   12 root      RT   0     0    0    0 S      0  0.0   0:00.02 watchdog/1                                                                                                                     
   13 root      RT   0     0    0    0 S      0  0.0   0:00.21 migration/2                                                                                                                    
   14 root      20   0     0    0    0 S      0  0.0   0:00.00 kworker/2:0                                                                                                                    
   15 root      20   0     0    0    0 S      0  0.0   0:00.02 ksoftirqd/2                                                                                                                    
   16 root      RT   0     0    0    0 S      0  0.0   0:00.01 watchdog/2                                                                                                                     
   17 root      RT   0     0    0    0 S      0  0.0   0:00.07 migration/3                                                                                                                    
   19 root      20   0     0    0    0 S      0  0.0   0:00.02 ksoftirqd/3                                                                                                                    
   20 root      RT   0     0    0    0 S      0  0.0   0:00.01 watchdog/3                                                                                                                     
   21 root       0 -20     0    0    0 S      0  0.0   0:00.00 cpuset                                                                                                                         
   22 root       0 -20     0    0    0 S      0  0.0   0:00.00 khelper                                                                                                                        
   23 root       0 -20     0    0    0 S      0  0.0   0:00.00 netns                                                                                                                          
   24 root      20   0     0    0    0 S      0  0.0   0:00.00 sync_supers                                                                                                                    
   25 root      20   0     0    0    0 S      0  0.0   0:00.00 bdi-default                                                                                                                    
   26 root       0 -20     0    0    0 S      0  0.0   0:00.00 kintegrityd                                                                                                                    
   27 root       0 -20     0    0    0 S      0  0.0   0:00.00 kblockd                                                                                                                        
   28 root       0 -20     0    0    0 S      0  0.0   0:00.00 md                                                                                                                             
   29 root      20   0     0    0    0 S      0  0.0   0:00.00 khungtaskd                                                                                                                     
   30 root      20   0     0    0    0 S      0  0.0   0:00.25 kswapd0                                                                                                                        
   31 root      25   5     0    0    0 S      0  0.0   0:00.00 ksmd                                                                                                                           
   32 root      39  19     0    0    0 S      0  0.0   0:00.06 khugepaged                                                                                                                     
   33 root      20   0     0    0    0 S      0  0.0   0:00.00 fsnotify_mark                                                                                                                  
   34 root       0 -20     0    0    0 S      0  0.0   0:00.00 crypto                                                                                                                         
   38 root       0 -20     0    0    0 S      0  0.0   0:00.00 kthrotld                                                                                                                       
   39 root       0 -20     0    0    0 S      0  0.0   0:00.00 kpsmoused                                                                                                                      
   84 root       0 -20     0    0    0 S      0  0.0   0:00.00 mpt_poll_0                                                                                                                     
   85 root       0 -20     0    0    0 S      0  0.0   0:00.00 mpt/0                                                                                                                          
   86 root      20   0     0    0    0 S      0  0.0   0:00.00 kworker/3:1                                                                                                                    
   87 root      20   0     0    0    0 S      0  0.0   0:03.38 kworker/2:1                                                                                                                    
   88 root      20   0     0    0    0 S      0  0.0   0:00.00 scsi_eh_0                                                                                                                      
   96 root       0 -20     0    0    0 S      0  0.0   0:00.00 ata_sff                                                                                                                        
   97 root      20   0     0    0    0 S      0  0.0   0:00.07 scsi_eh_1                                                                                                                      
   98 root      20   0     0    0    0 S      0  0.0   0:00.08 scsi_eh_2                                                                                                                      
   99 root      20   0     0    0    0 S      0  0.0   0:00.00 scsi_eh_3                                                                                                                      
  100 root      20   0     0    0    0 S      0  0.0   0:00.00 scsi_eh_4                                                                                                                      
  101 root      20   0     0    0    0 S      0  0.0   0:00.00 scsi_eh_5                                                                                                                      
  102 root      20   0     0    0    0 S      0  0.0   0:00.00 scsi_eh_6                                                                                                                      
  103 root      20   0     0    0    0 S      0  0.0   0:00.00 scsi_eh_7                                                                                                                      
  104 root      20   0     0    0    0 S      0  0.0   0:00.00 scsi_eh_8                                                                                                                      
cotter@linux-rlsj:~/gitlocalrepo/salt-install/Install-Scripts/sles_11> 
cotter@linux-rlsj:~/gitlocalrepo/salt-install/Install-Scripts/sles_11> df
Filesystem     1K-blocks    Used Available Use% Mounted on
/dev/sda2       29461480 4107352  23857552  15% /
udev              507060     124    506936   1% /dev
tmpfs             507060      96    506964   1% /dev/shm
/dev/sr0         3304920 3304920         0 100% /media/SLES-11-SP4-DVD-x86_6412211
cotter@linux-rlsj:~/gitlocalrepo/salt-install/Install-Scripts/sles_11> 


```


Remarks:

CPU salt-minion to 0.6 % when accessed by the Master, not anymore, otherwise,
no impact. 

- Ram
- CPU
- Disk space

All is ok.

__Validate !__