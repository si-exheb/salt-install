# Tested and validated OSes for the Minion install #

## List of machines in TEST environnement: ##

### Windows2012 R2 64 bits ###

Machine name: __ditexmssqlstud.epfl.ch__ (MS SQL test machine)

#### Initial checks: ####

```

Used RAM
1700 MB

Free RAM
2300 MB

CPU average
4%

Processes Nb.
52

Network usage
100 Kbits

```

#### Application of install procedure: ####
- done !

#### Post-install checks: ####

```

Used RAM
1800 MB

Free RAM
2200 MB

CPU average
5%

Processes Nb.
53

Network usage
110 Kbits

```



Remarks:

CPU salt-minion to 1.2 % when accessed by the Master, not anymore, otherwise,
no impact. 

- Ram
- CPU
- Disk space


All is ok.

__Validate !__