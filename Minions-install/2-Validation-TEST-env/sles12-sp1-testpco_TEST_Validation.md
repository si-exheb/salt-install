# Tested and validated OSes for the Minion install #

## List of machines in TEST environnement: ##

### SLES 12 (Suse Linux Entreprise Server - SP1)###

Machine name: __sles12-sp1-testpco.epfl.ch__ (VM test machine)

Intial checks:

```

cotter@sles12-sp1-testpco:~> cat /etc/issue

Welcome to SUSE Linux Enterprise Server 12 SP1  (x86_64) - Kernel \r (\l).


cotter@sles12-sp1-testpco:~> top

top - 14:53:15 up  5:01,  3 users,  load average: 0.00, 0.01, 0.05
Tasks: 218 total,   1 running, 217 sleeping,   0 stopped,   0 zombie
%Cpu(s):  0.1 us,  0.1 sy,  0.0 ni, 99.8 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
KiB Mem:   1012528 total,   788816 used,   223712 free,       40 buffers
KiB Swap:  1517564 total,     2816 used,  1514748 free.   343672 cached Mem

  PID USER      PR  NI    VIRT    RES    SHR S  %CPU  %MEM     TIME+ COMMAND                                                         
 1274 root      20   0  267640  36540  10592 S 6.667 3.609   0:25.01 Xorg                                                            
    1 root      20   0   34044   4472   2144 S 0.000 0.442   0:03.64 systemd                                                         
    2 root      20   0       0      0      0 S 0.000 0.000   0:00.05 kthreadd                                                        
    3 root      20   0       0      0      0 S 0.000 0.000   0:00.15 ksoftirqd/0                                                     
    5 root       0 -20       0      0      0 S 0.000 0.000   0:00.00 kworker/0:0H                                                    
    7 root      rt   0       0      0      0 S 0.000 0.000   0:00.74 migration/0                                                     
    8 root      20   0       0      0      0 S 0.000 0.000   0:00.00 rcu_bh                                                          
    9 root      20   0       0      0      0 S 0.000 0.000   0:02.71 rcu_sched                                                       
   10 root      rt   0       0      0      0 S 0.000 0.000   0:00.14 watchdog/0                                                      
   11 root      rt   0       0      0      0 S 0.000 0.000   0:00.21 watchdog/1                                                      
   12 root      rt   0       0      0      0 S 0.000 0.000   0:00.64 migration/1                                                     
   13 root      20   0       0      0      0 S 0.000 0.000   0:00.30 ksoftirqd/1                                                     
   15 root       0 -20       0      0      0 S 0.000 0.000   0:00.00 kworker/1:0H                                                    
   16 root      rt   0       0      0      0 S 0.000 0.000   0:00.15 watchdog/2                                                      
   17 root      rt   0       0      0      0 S 0.000 0.000   0:00.74 migration/2                                                     
   18 root      20   0       0      0      0 S 0.000 0.000   0:00.06 ksoftirqd/2                                                     
   20 root       0 -20       0      0      0 S 0.000 0.000   0:00.00 kworker/2:0H                                                    
   21 root      rt   0       0      0      0 S 0.000 0.000   0:00.14 watchdog/3                                                      
   22 root      rt   0       0      0      0 S 0.000 0.000   0:00.44 migration/3                                                     
   23 root      20   0       0      0      0 S 0.000 0.000   0:00.12 ksoftirqd/3                                                     
   25 root       0 -20       0      0      0 S 0.000 0.000   0:00.00 kworker/3:0H                                                    
   26 root       0 -20       0      0      0 S 0.000 0.000   0:00.00 khelper                                                         
   27 root      20   0       0      0      0 S 0.000 0.000   0:00.00 kdevtmpfs                                                       
   28 root       0 -20       0      0      0 S 0.000 0.000   0:00.00 netns                                                           
   29 root       0 -20       0      0      0 S 0.000 0.000   0:00.00 perf                                                            
   30 root       0 -20       0      0      0 S 0.000 0.000   0:00.00 writeback                                                       
   31 root       0 -20       0      0      0 S 0.000 0.000   0:00.00 kintegrityd                                                     
   32 root       0 -20       0      0      0 S 0.000 0.000   0:00.00 bioset                                                          
   33 root       0 -20       0      0      0 S 0.000 0.000   0:00.00 crypto                                                          
   34 root       0 -20       0      0      0 S 0.000 0.000   0:00.00 kblockd                                                         
   39 root       0 -20       0      0      0 S 0.000 0.000   0:00.00 kgraft                                                          
   40 root      20   0       0      0      0 S 0.000 0.000   0:00.00 khungtaskd                                                      
   41 root      20   0       0      0      0 S 0.000 0.000   0:00.16 kswapd0                                                         
   42 root      25   5       0      0      0 S 0.000 0.000   0:00.00 ksmd                                                            
   43 root      39  19       0      0      0 S 0.000 0.000   0:00.38 khugepaged                                                      
   44 root      20   0       0      0      0 S 0.000 0.000   0:00.00 fsnotify_mark                                                   
   54 root       0 -20       0      0      0 S 0.000 0.000   0:00.00 kthrotld                                                        
   55 root       0 -20       0      0      0 S 0.000 0.000   0:00.00 kpsmoused                                                       
   57 root      20   0       0      0      0 S 0.000 0.000   0:00.00 print/0                                                         
   58 root      20   0       0      0      0 S 0.000 0.000   0:00.00 print/1                                                         
cotter@sles12-sp1-testpco:~> df
Filesystem     1K-blocks    Used Available Use% Mounted on
/dev/sda2       12585984 3264256   9136320  27% /
devtmpfs          497208       0    497208   0% /dev
tmpfs             506264     144    506120   1% /dev/shm
tmpfs             506264    8156    498108   2% /run
tmpfs             506264       0    506264   0% /sys/fs/cgroup
/dev/sda2       12585984 3264256   9136320  27% /.snapshots
/dev/sda2       12585984 3264256   9136320  27% /var/opt
/dev/sda2       12585984 3264256   9136320  27% /var/spool
/dev/sda2       12585984 3264256   9136320  27% /var/tmp
/dev/sda2       12585984 3264256   9136320  27% /var/lib/mariadb
/dev/sda2       12585984 3264256   9136320  27% /var/log
/dev/sda2       12585984 3264256   9136320  27% /var/lib/named
/dev/sda2       12585984 3264256   9136320  27% /var/lib/mailman
/dev/sda2       12585984 3264256   9136320  27% /var/lib/pgsql
/dev/sda2       12585984 3264256   9136320  27% /var/crash
/dev/sda2       12585984 3264256   9136320  27% /var/lib/mysql
/dev/sda2       12585984 3264256   9136320  27% /var/lib/libvirt/images
/dev/sda2       12585984 3264256   9136320  27% /tmp
/dev/sda2       12585984 3264256   9136320  27% /usr/local
/dev/sda2       12585984 3264256   9136320  27% /srv
/dev/sda2       12585984 3264256   9136320  27% /opt
/dev/sda2       12585984 3264256   9136320  27% /boot/grub2/i386-pc
/dev/sda2       12585984 3264256   9136320  27% /boot/grub2/x86_64-efi
/dev/sda3       17342464   33

```

Install of wget& git

```

cotter@sles12-sp1-testpco:~> sudo zypper install git-core
root's password:
Loading repository data...
Reading installed packages...
Resolving package dependencies...

The following 2 NEW packages are going to be installed:
  git-core perl-Error

2 new packages to install.
Overall download size: 3.2 MiB. Already cached: 0 B. After the operation, additional 19.5 MiB will be used.
Continue? [y/n/? shows all options] (y): y
Retrieving package perl-Error-0.17021-1.18.noarch                                               (1/2),  28.3 KiB ( 49.8 KiB unpacked)
Retrieving package git-core-1.8.5.6-11.10.x86_64                                                (2/2),   3.2 MiB ( 19.4 MiB unpacked)
Checking for file conflicts: ..................................................................................................[done]
(1/2) Installing: perl-Error-0.17021-1.18 .....................................................................................[done]
(2/2) Installing: git-core-1.8.5.6-11.10 ......................................................................................[done]
cotter@sles12-sp1-testpco:~> sudo zypper install wget
Loading repository data...
Reading installed packages...
'wget' is already installed.
No update candidate for 'wget-1.14-7.1.x86_64'. The highest available version is already installed.
Resolving package dependencies...

Nothing to do.

```

Install of Salt-minion via shell script

Post install checks:

```

cotter@sles12-sp1-testpco:~/gitlocalrepo/salt-install/Install-Scripts/sles_12> cat /etc/issue

Welcome to SUSE Linux Enterprise Server 12 SP1  (x86_64) - Kernel \r (\l).


cotter@sles12-sp1-testpco:~/gitlocalrepo/salt-install/Install-Scripts/sles_12> top

top - 15:53:44 up  6:02,  3 users,  load average: 0.01, 0.02, 0.05
Tasks: 225 total,   1 running, 224 sleeping,   0 stopped,   0 zombie
%Cpu(s):  0.7 us,  0.3 sy,  0.0 ni, 99.0 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
KiB Mem:   1012528 total,   935700 used,    76828 free,      424 buffers
KiB Swap:  1517564 total,    45532 used,  1472032 free.   259304 cached Mem

  PID USER      PR  NI    VIRT    RES    SHR S  %CPU  %MEM     TIME+ COMMAND                                                         
 8256 cotter    20   0 1042636 217912  23516 S 1.656 21.52   2:28.26 firefox                                                         
 7837 cotter    20   0  406744  16432   7956 S 0.993 1.623   0:23.43 gnome-terminal-                                                 
  603 root      20   0   12032    672    580 S 0.331 0.066   0:06.34 haveged                                                         
  987 root      20   0       0      0      0 S 0.331 0.000   0:01.28 xfsaild/sda3                                                    
 1171 root      20   0  153040   2880   2440 S 0.331 0.284   0:30.58 vmtoolsd                                                        
 2066 cotter    20   0  542064   3392   2668 S 0.331 0.335   0:00.33 gnome-session                                                   
 2112 cotter    20   0   34480   1408    816 S 0.331 0.139   0:00.32 dbus-daemon                                                     
 2144 cotter    20   0  371176   4204   2744 S 0.331 0.415   0:02.71 pulseaudio                                                      
 2197 cotter    20   0 1861268 249248  17256 S 0.331 24.62   1:16.88 gnome-shell                                                     
12519 cotter    20   0   15340   1712   1116 R 0.331 0.169   0:00.02 top                                                             
    1 root      20   0   34044   4068   1948 S 0.000 0.402   0:04.07 systemd                                                         
    2 root      20   0       0      0      0 S 0.000 0.000   0:00.06 kthreadd                                                        
    3 root      20   0       0      0      0 S 0.000 0.000   0:00.27 ksoftirqd/0                                                     
    5 root       0 -20       0      0      0 S 0.000 0.000   0:00.00 kworker/0:0H                                                    
    7 root      rt   0       0      0      0 S 0.000 0.000   0:00.76 migration/0                                                     
    8 root      20   0       0      0      0 S 0.000 0.000   0:00.00 rcu_bh                                                          
    9 root      20   0       0      0      0 S 0.000 0.000   0:05.46 rcu_sched                                                       
   10 root      rt   0       0      0      0 S 0.000 0.000   0:00.19 watchdog/0                                                      
   11 root      rt   0       0      0      0 S 0.000 0.000   0:00.26 watchdog/1                                                      
   12 root      rt   0       0      0      0 S 0.000 0.000   0:00.64 migration/1                                                     
   13 root      20   0       0      0      0 S 0.000 0.000   0:01.38 ksoftirqd/1                                                     
   15 root       0 -20       0      0      0 S 0.000 0.000   0:00.00 kworker/1:0H                                                    
   16 root      rt   0       0      0      0 S 0.000 0.000   0:00.20 watchdog/2                                                      
   17 root      rt   0       0      0      0 S 0.000 0.000   0:00.76 migration/2                                                     
   18 root      20   0       0      0      0 S 0.000 0.000   0:00.23 ksoftirqd/2                                                     
   20 root       0 -20       0      0      0 S 0.000 0.000   0:00.00 kworker/2:0H                                                    
   21 root      rt   0       0      0      0 S 0.000 0.000   0:00.18 watchdog/3                                                      
   22 root      rt   0       0      0      0 S 0.000 0.000   0:00.45 migration/3                                                     
   23 root      20   0       0      0      0 S 0.000 0.000   0:00.48 ksoftirqd/3                                                     
   25 root       0 -20       0      0      0 S 0.000 0.000   0:00.00 kworker/3:0H                                                    
   26 root       0 -20       0      0      0 S 0.000 0.000   0:00.00 khelper                                                         
   27 root      20   0       0      0      0 S 0.000 0.000   0:00.00 kdevtmpfs                                                       
   28 root       0 -20       0      0      0 S 0.000 0.000   0:00.00 netns                                                           
   29 root       0 -20       0      0      0 S 0.000 0.000   0:00.00 perf                                                            
   30 root       0 -20       0      0      0 S 0.000 0.000   0:00.00 writeback                                                       
   31 root       0 -20       0      0      0 S 0.000 0.000   0:00.00 kintegrityd                                                     
   32 root       0 -20       0      0      0 S 0.000 0.000   0:00.00 bioset                                                          
   33 root       0 -20       0      0      0 S 0.000 0.000   0:00.00 crypto                                                          
   34 root       0 -20       0      0      0 S 0.000 0.000   0:00.00 kblockd                                                         
   39 root       0 -20       0      0      0 S 0.000 0.000   0:00.00 kgraft                                                          
   40 root      20   0       0      0      0 S 0.000 0.000   0:00.00 khungtaskd                                                      
   41 root      20   0       0      0      0 S 0.000 0.000   0:01.06 kswapd0                                                         
   42 root      25   5       0      0      0 S 0.000 0.000   0:00.00 ksmd                                                            
   43 root      39  19       0      0      0 S 0.000 0.000   0:00.44 khugepaged                                                      
   44 root      20   0       0      0      0 S 0.000 0.000   0:00.00 fsnotify_mark                                                   
   54 root       0 -20       0      0      0 S 0.000 0.000   0:00.00 kthrotld                                                        
cotter@sles12-sp1-testpco:~/gitlocalrepo/salt-install/Install-Scripts/sles_12> df
Filesystem     1K-blocks    Used Available Use% Mounted on
/dev/sda2       12585984 3613448   8629208  30% /
devtmpfs          497208       0    497208   0% /dev
tmpfs             506264     160    506104   1% /dev/shm
tmpfs             506264    8156    498108   2% /run
tmpfs             506264       0    506264   0% /sys/fs/cgroup
/dev/sda2       12585984 3613448   8629208  30% /.snapshots
/dev/sda2       12585984 3613448   8629208  30% /var/opt
/dev/sda2       12585984 3613448   8629208  30% /var/spool
/dev/sda2       12585984 3613448   8629208  30% /var/tmp
/dev/sda2       12585984 3613448   8629208  30% /var/lib/mariadb
/dev/sda2       12585984 3613448   8629208  30% /var/log
/dev/sda2       12585984 3613448   8629208  30% /var/lib/named
/dev/sda2       12585984 3613448   8629208  30% /var/lib/mailman
/dev/sda2       12585984 3613448   8629208  30% /var/lib/pgsql
/dev/sda2       12585984 3613448   8629208  30% /var/crash
/dev/sda2       12585984 3613448   8629208  30% /var/lib/mysql
/dev/sda2       12585984 3613448   8629208  30% /var/lib/libvirt/images
/dev/sda2       12585984 3613448   8629208  30% /tmp
/dev/sda2       12585984 3613448   8629208  30% /usr/local
/dev/sda2       12585984 3613448   8629208  30% /srv
/dev/sda2       12585984 3613448   8629208  30% /opt
/dev/sda2       12585984 3613448   8629208  30% /boot/grub2/i386-pc
/dev/sda2       12585984 3613448   8629208  30% /boot/grub2/x86_64-efi
/dev/sda3       17342464   57336  17285128   1% /home
cotter@sles12-sp1-testpco:~/gitlocalrepo/salt-install/Install-Scripts/sles_12> 

```


Remarks:

CPU salt-minion to 0.8 % when accessed by the Master, not anymore, otherwise,
no impact. 

- Ram
- CPU
- Disk space

All is ok.

__Validate !__