# Tested and validated OSes for the Minion install #

## List of machines in TEST environnement: ##

### CentOS 7 ###

Machine name: __exhebdb04.epfl.ch__ (MariaDB TEST machine)

Intial checks:

```

[root@exhebdb04 home]# cat /etc/system-release
CentOS Linux release 7.1.1503 (Core)

[root@exhebdb04 home]# top
top - 08:05:11 up 196 days, 18:41,  1 user,  load average: 0.00, 0.01, 0.05
Tasks: 237 total,   1 running, 236 sleeping,   0 stopped,   0 zombie
%Cpu(s):  0.1 us,  0.2 sy,  0.0 ni, 99.8 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
KiB Mem :  3876012 total,   239524 free,  3307896 used,   328592 buff/cache
KiB Swap:  1048572 total,   692960 free,   355612 used.   317596 avail Mem

  PID USER      PR  NI    VIRT    RES    SHR S  %CPU %MEM     TIME+ COMMAND
10079 root      20   0  130156   1864   1204 R   0.7  0.0   0:00.07 top
    1 root      20   0   56748   4636   2584 S   0.0  0.1  76:30.32 systemd
    2 root      20   0       0      0      0 S   0.0  0.0   0:04.52 kthreadd
    3 root      20   0       0      0      0 S   0.0  0.0   4:04.09 ksoftirqd/0
    5 root       0 -20       0      0      0 S   0.0  0.0   0:00.00 kworker/0:0H
    7 root      rt   0       0      0      0 S   0.0  0.0   0:09.34 migration/0
    8 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcu_bh
    9 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/0
   10 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/1
   11 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/2
   12 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/3
   13 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/4
   14 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/5
   15 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/6
   16 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/7
   17 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/8
   18 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/9
   19 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/10
   20 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/11
   21 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/12
   22 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/13
   23 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/14
   24 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/15
   25 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/16
   26 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/17
   27 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/18
   28 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/19
   29 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/20
   30 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/21
   31 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/22
   32 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/23
   33 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/24
   34 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/25
   35 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/26
   36 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/27
   37 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/28
   
[root@exhebdb04 home]# df
Filesystem              1K-blocks    Used Available Use% Mounted on
/dev/mapper/centos-root   8910848 2860364   6050484  33% /
devtmpfs                  1928616       0   1928616   0% /dev
tmpfs                     1938004       0   1938004   0% /dev/shm
tmpfs                     1938004  186720   1751284  10% /run
tmpfs                     1938004       0   1938004   0% /sys/fs/cgroup
/dev/sda1                  508588  231408    277180  46% /boot
[root@exhebdb04 home]#


```

Install of wget:

```

[root@exhebdb04 home]# yum install wget
Loaded plugins: fastestmirror
MariaDB                                                                                      | 2.9 kB  00:00:00
base                                                                                         | 3.6 kB  00:00:00
epel/x86_64/metalink                                                                         |  25 kB  00:00:00
epel                                                                                         | 4.3 kB  00:00:00
extras                                                                                       | 3.4 kB  00:00:00
saltstack-repo                                                                               | 2.9 kB  00:00:00
updates                                                                                      | 3.4 kB  00:00:00
vmware-tools                                                                                 |  951 B  00:00:00
epel/x86_64/primary_db         FAILED
http://mirror.netcologne.de/fedora-epel/7/x86_64/repodata/5816772f98516737b60eb748f0064b8a4444f68db8c8ed9096ba9c8fb14f4800-primary.sqlite.xz: [Errno 14] HTTP Error 404 - Not Found
Trying other mirror.
(1/3): epel/x86_64/updateinfo                                                                | 510 kB  00:00:00
(2/3): updates/7/x86_64/primary_db                                                           | 3.1 MB  00:00:00
(3/3): epel/x86_64/primary_db                                                                | 3.9 MB  00:00:00
Loading mirror speeds from cached hostfile
 * base: linuxsoft.cern.ch
 * epel: fr2.rpmfind.net
 * extras: linuxsoft.cern.ch
 * updates: mirror.switch.ch
Package wget-1.14-10.el7_0.1.x86_64 already installed and latest version
Nothing to do


```

Already installed - OK !


Install of git:

```

[root@exhebdb04 home]# yum install git
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
 * base: linuxsoft.cern.ch
 * epel: fr2.rpmfind.net
 * extras: linuxsoft.cern.ch
 * updates: mirror.switch.ch
Package git-1.8.3.1-6.el7.x86_64 already installed and latest version
Nothing to do


```

Already installed - OK !



Post install checks:


```

[root@exhebdb04 centos-rhel_7]# cat /etc/system-release
CentOS Linux release 7.1.1503 (Core)

[root@exhebdb04 centos-rhel_7]# top
top - 08:17:12 up 196 days, 18:53,  1 user,  load average: 0.01, 0.04, 0.05
Tasks: 240 total,   2 running, 238 sleeping,   0 stopped,   0 zombie
%Cpu(s):  0.1 us,  0.1 sy,  0.0 ni, 99.8 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
KiB Mem :  3876012 total,   160108 free,  3344140 used,   371764 buff/cache
KiB Swap:  1048572 total,   693012 free,   355560 used.   273532 avail Mem

  PID USER      PR  NI    VIRT    RES    SHR S  %CPU %MEM     TIME+ COMMAND
10447 root      20   0  130156   1868   1200 R   0.3  0.0   0:00.11 top
    1 root      20   0   56748   4636   2584 S   0.0  0.1  76:30.72 systemd
    2 root      20   0       0      0      0 S   0.0  0.0   0:04.52 kthreadd
    3 root      20   0       0      0      0 S   0.0  0.0   4:04.10 ksoftirqd/0
    5 root       0 -20       0      0      0 S   0.0  0.0   0:00.00 kworker/0:0H
    7 root      rt   0       0      0      0 S   0.0  0.0   0:09.35 migration/0
    8 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcu_bh
    9 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/0
   10 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/1
   11 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/2
   12 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/3
   13 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/4
   14 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/5
   15 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/6
   16 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/7
   17 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/8
   18 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/9
   19 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/10
   20 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/11
   21 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/12
   22 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/13
   23 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/14
   24 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/15
   25 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/16
   26 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/17
   27 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/18
   28 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/19
   29 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/20
   30 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/21
   31 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/22
   32 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/23
   33 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/24
   34 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/25
   35 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/26
   36 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/27
   37 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcuob/28
[root@exhebdb04 centos-rhel_7]# df

Filesystem              1K-blocks    Used Available Use% Mounted on
/dev/mapper/centos-root   8910848 2889432   6021416  33% /
devtmpfs                  1928616       0   1928616   0% /dev
tmpfs                     1938004      12   1937992   1% /dev/shm
tmpfs                     1938004  186720   1751284  10% /run
tmpfs                     1938004       0   1938004   0% /sys/fs/cgroup
/dev/sda1                  508588  231408    277180  46% /boot
[root@exhebdb04 centos-rhel_7]#


```


Remarks:

CPU salt-minion to 1.2 % when accessed by the Master, not anymore, otherwise,
no impact. 

- Ram
- CPU
- Disk space
- MySQL deamon and DB


All is ok.

__Validate !__