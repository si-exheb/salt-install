# Tested and validated OSes for the Minion install #

## List of machines in TEST environnement: ##

### CentOS 6 (6.7) ###

Machine name: __bkp-test-vtl__ (Virtual Tape Library)

Intial checks:

```

*test*[cotter@bkp-test-vtl ~]$ cat /etc/system-release
Red Hat Enterprise Linux Server release 6.7 (Santiago)
*test*[cotter@bkp-test-vtl ~]$ top

top - 12:57:35 up  3:45,  1 user,  load average: 0.00, 0.00, 0.00
Tasks: 119 total,   1 running, 118 sleeping,   0 stopped,   0 zombie
Cpu(s):  0.0%us,  0.0%sy,  0.0%ni,  0.0%id,  0.0%wa,  0.0%hi,  0.0%si,  0.0%st
Mem:   1914592k total,   563916k used,  1350676k free,    32620k buffers
Swap:  1048572k total,        0k used,  1048572k free,   261196k cached

  PID USER      PR  NI  VIRT  RES  SHR S %CPU %MEM    TIME+  COMMAND                          
    1 root      20   0 21456 1580 1260 S  0.0  0.1   0:01.25 init                             
    2 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kthreadd                         
    3 root      RT   0     0    0    0 S  0.0  0.0   0:00.00 migration/0                      
    4 root      20   0     0    0    0 S  0.0  0.0   0:00.03 ksoftirqd/0                      
    5 root      RT   0     0    0    0 S  0.0  0.0   0:00.00 stopper/0                        
    6 root      RT   0     0    0    0 S  0.0  0.0   0:00.01 watchdog/0                       
    7 root      20   0     0    0    0 S  0.0  0.0   0:00.71 events/0                         
    8 root      20   0     0    0    0 S  0.0  0.0   0:00.00 events/0                         
    9 root      20   0     0    0    0 S  0.0  0.0   0:00.00 events_long/0                    
   10 root      20   0     0    0    0 S  0.0  0.0   0:00.00 events_power_ef                  
   11 root      20   0     0    0    0 S  0.0  0.0   0:00.00 cgroup                           
   12 root      20   0     0    0    0 S  0.0  0.0   0:00.00 khelper                          
   13 root      20   0     0    0    0 S  0.0  0.0   0:00.00 netns                            
   14 root      20   0     0    0    0 S  0.0  0.0   0:00.00 async/mgr                        
   15 root      20   0     0    0    0 S  0.0  0.0   0:00.00 pm                               
   16 root      20   0     0    0    0 S  0.0  0.0   0:00.03 sync_supers                      
   17 root      20   0     0    0    0 S  0.0  0.0   0:00.04 bdi-default                      
   18 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kintegrityd/0                    
   19 root      20   0     0    0    0 S  0.0  0.0   0:00.13 kblockd/0                        
   20 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kacpid                           
   21 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kacpi_notify                     
   22 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kacpi_hotplug                    
   23 root      20   0     0    0    0 S  0.0  0.0   0:00.00 ata_aux                          
   24 root      20   0     0    0    0 S  0.0  0.0   0:00.00 ata_sff/0                        
   25 root      20   0     0    0    0 S  0.0  0.0   0:00.00 ksuspend_usbd                    
   26 root      20   0     0    0    0 S  0.0  0.0   0:00.00 khubd                            
   27 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kseriod                          
   28 root      20   0     0    0    0 S  0.0  0.0   0:00.00 md/0                             
   29 root      20   0     0    0    0 S  0.0  0.0   0:00.00 md_misc/0                        
   30 root      20   0     0    0    0 S  0.0  0.0   0:00.00 linkwatch                        
   31 root      20   0     0    0    0 S  0.0  0.0   0:00.00 khungtaskd                       
   32 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kswapd0                          
   33 root      25   5     0    0    0 S  0.0  0.0   0:00.00 ksmd                             
   34 root      39  19     0    0    0 S  0.0  0.0   0:00.05 khugepaged                       
   35 root      20   0     0    0    0 S  0.0  0.0   0:00.00 aio/0                            
   36 root      20   0     0    0    0 S  0.0  0.0   0:00.00 crypto/0                         
   43 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kthrotld/0                       
   44 root      20   0     0    0    0 S  0.0  0.0   0:00.00 pciehpd                          
   46 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kpsmoused                        
   47 root      20   0     0    0    0 S  0.0  0.0   0:00.00 usbhid_resumer                   
   48 root      20   0     0    0    0 S  0.0  0.0   0:00.00 deferwq                          
   81 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kdmremove                        
   82 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kstriped                         
  185 root      20   0     0    0    0 S  0.0  0.0   0:00.01 scsi_eh_0                        
  186 root      20   0     0    0    0 S  0.0  0.0   0:00.00 scsi_eh_1                        
  195 root      20   0     0    0    0 S  0.0  0.0   0:00.00 scsi_eh_2                        
  196 root      20   0     0    0    0 S  0.0  0.0   0:00.00 vmw_pvscsi_wq_2                  
  302 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kdmflush                         
  304 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kdmflush                         
  370 root      20   0     0    0    0 S  0.0  0.0   0:00.08 jbd2/dm-0-8                      
  371 root      20   0     0    0    0 S  0.0  0.0   0:00.00 ext4-dio-unwrit                  

*test*[cotter@bkp-test-vtl ~]$ df
Filesystem           1K-blocks      Used Available Use% Mounted on
/dev/mapper/VolGroup-lv_root
                      35485840   7583476  26093376  23% /
tmpfs                   957296         4    957292   1% /dev/shm
/dev/sda1               487652    104000    358052  23% /boot
nas-app-ma-nfs2:/si_bkp_test_vtl_app
                     524707456 164076992 360630464  32% /opt/mhvtl
*test*[cotter@bkp-test-vtl ~]$


```

Install of wget:

```

*test*[cotter@bkp-test-vtl ~]$ sudo yum install wget
[sudo] password for cotter: 
Loaded plugins: product-id, refresh-packagekit, security, subscription-manager
Setting up Install Process
Package wget-1.12-5.el6_6.1.x86_64 already installed and latest version
Nothing to do
*test*[cotter@bkp-test-vtl ~]$

```

Already installed - OK !


Install of git:

```

*test*[cotter@bkp-test-vtl ~]$ sudo yum install git
Loaded plugins: product-id, refresh-packagekit, security, subscription-manager
Setting up Install Process
Package git-1.7.1-3.el6_4.1.x86_64 already installed and latest version
Nothing to do
*test*[cotter@bkp-test-vtl ~]$ 

```

Already installed - OK !



Installation process of the script.



Post install checks:


```
*test*[cotter@bkp-test-vtl centos-rhel_6]$ top





































top - 13:07:36 up  3:55,  1 user,  load average: 0.07, 0.10, 0.04
Tasks: 121 total,   1 running, 120 sleeping,   0 stopped,   0 zombie
Cpu(s):  0.0%us,  0.3%sy,  0.0%ni, 99.7%id,  0.0%wa,  0.0%hi,  0.0%si,  0.0%st
Mem:   1914592k total,   778036k used,  1136556k free,    55956k buffers
Swap:  1048572k total,        0k used,  1048572k free,   397480k cached

  PID USER      PR  NI  VIRT  RES  SHR S %CPU %MEM    TIME+  COMMAND                          
    7 root      20   0     0    0    0 S  0.3  0.0   0:00.76 events/0                         
 2679 root      20   0  259m  10m 8416 S  0.3  0.5   0:31.33 .tina_daemon.re                  
    1 root      20   0 21456 1584 1260 S  0.0  0.1   0:01.25 init                             
    2 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kthreadd                         
    3 root      RT   0     0    0    0 S  0.0  0.0   0:00.00 migration/0                      
    4 root      20   0     0    0    0 S  0.0  0.0   0:00.03 ksoftirqd/0                      
    5 root      RT   0     0    0    0 S  0.0  0.0   0:00.00 stopper/0                        
    6 root      RT   0     0    0    0 S  0.0  0.0   0:00.01 watchdog/0                       
    8 root      20   0     0    0    0 S  0.0  0.0   0:00.00 events/0                         
    9 root      20   0     0    0    0 S  0.0  0.0   0:00.00 events_long/0                    
   10 root      20   0     0    0    0 S  0.0  0.0   0:00.00 events_power_ef                  
   11 root      20   0     0    0    0 S  0.0  0.0   0:00.00 cgroup                           
   12 root      20   0     0    0    0 S  0.0  0.0   0:00.00 khelper                          
   13 root      20   0     0    0    0 S  0.0  0.0   0:00.00 netns                            
   14 root      20   0     0    0    0 S  0.0  0.0   0:00.00 async/mgr                        
   15 root      20   0     0    0    0 S  0.0  0.0   0:00.00 pm                               
   16 root      20   0     0    0    0 S  0.0  0.0   0:00.04 sync_supers                      
   17 root      20   0     0    0    0 S  0.0  0.0   0:00.04 bdi-default                      
   18 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kintegrityd/0                    
   19 root      20   0     0    0    0 S  0.0  0.0   0:00.19 kblockd/0                        
   20 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kacpid                           
   21 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kacpi_notify                     
   22 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kacpi_hotplug                    
   23 root      20   0     0    0    0 S  0.0  0.0   0:00.00 ata_aux                          
   24 root      20   0     0    0    0 S  0.0  0.0   0:00.00 ata_sff/0                        
   25 root      20   0     0    0    0 S  0.0  0.0   0:00.00 ksuspend_usbd                    
   26 root      20   0     0    0    0 S  0.0  0.0   0:00.00 khubd                            
   27 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kseriod                          
   28 root      20   0     0    0    0 S  0.0  0.0   0:00.00 md/0                             
   29 root      20   0     0    0    0 S  0.0  0.0   0:00.00 md_misc/0                        
   30 root      20   0     0    0    0 S  0.0  0.0   0:00.00 linkwatch                        
   31 root      20   0     0    0    0 S  0.0  0.0   0:00.00 khungtaskd                       
   32 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kswapd0                          
   33 root      25   5     0    0    0 S  0.0  0.0   0:00.00 ksmd                             
   34 root      39  19     0    0    0 S  0.0  0.0   0:00.05 khugepaged                       
   35 root      20   0     0    0    0 S  0.0  0.0   0:00.00 aio/0                            
   36 root      20   0     0    0    0 S  0.0  0.0   0:00.00 crypto/0                         
   43 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kthrotld/0                       
   44 root      20   0     0    0    0 S  0.0  0.0   0:00.00 pciehpd                          
   46 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kpsmoused                        
   47 root      20   0     0    0    0 S  0.0  0.0   0:00.00 usbhid_resumer                   
   48 root      20   0     0    0    0 S  0.0  0.0   0:00.00 deferwq                          
   81 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kdmremove                        
   82 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kstriped                         
  185 root      20   0     0    0    0 S  0.0  0.0   0:00.01 scsi_eh_0                        
  186 root      20   0     0    0    0 S  0.0  0.0   0:00.00 scsi_eh_1                        
  195 root      20   0     0    0    0 S  0.0  0.0   0:00.00 scsi_eh_2                        
  196 root      20   0     0    0    0 S  0.0  0.0   0:00.00 vmw_pvscsi_wq_2                  
  302 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kdmflush                         
  304 root      20   0     0    0    0 S  0.0  0.0   0:00.00 kdmflush                         
  370 root      20   0     0    0    0 S  0.0  0.0   0:00.10 jbd2/dm-0-8                      
*test*[cotter@bkp-test-vtl centos-rhel_6]$ df
Filesystem           1K-blocks      Used Available Use% Mounted on
/dev/mapper/VolGroup-lv_root
                      35485840   7636928  26039924  23% /
tmpfs                   957296        16    957280   1% /dev/shm
/dev/sda1               487652    104000    358052  23% /boot
nas-app-ma-nfs2:/si_bkp_test_vtl_app
                     524707456 164076992 360630464  32% /opt/mhvtl
*test*[cotter@bkp-test-vtl centos-rhel_6]$


```


Remarks:

CPU salt-minion to 1.4 % when accessed by the Master, not anymore, otherwise,
no impact. 

- Ram
- CPU
- Disk space
- mhvtl ok

All is ok.

__Validate !__

