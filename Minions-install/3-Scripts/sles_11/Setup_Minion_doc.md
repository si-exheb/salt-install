# Setup Documentation for a minion on SUSE Linux Enterprise Server (SLES) version 11 - SP4 OS#

Please use the setup-salt-minion-sles11.sh.

## On the target server: ##

### 1. Connect on the target server ###

```
ssh username@targetserver
```

### 2. Verifiy the sudoing capability. ###

```
sudo su -
```

After credentials returned, if the return prompt is root@targetmachinename, it's 
all ok to continue.

```
exit
```

To go back in normal user mode.

### 3. install git and wget tools ###

Wget is totally non-invasive and have no dependencies. Totally safe to install.

Git is non-invasive, it have only dependencies of pearl libs. Please
check on the target server if it could disturb or not.

Please adapt the OS version to the 2 commande lines following:
Change SLE_11_SP4 with the good one...

```
sudo zypper addrepo http://download.opensuse.org/repositories/devel:/tools:/scm/SLE_11_SP4/devel:tools:scm.repo
sudo zypper addrepo http://download.opensuse.org/repositories/devel:/languages:/perl/SLE_11_SP4/devel:languages:perl.repo

sudo zypper install git-core
sudo zypper install wget
```

### 4. go on the username's home and git clone the project ###

Use your username and password for gitlab.epfl.ch when you pull the
project.

```
cd $home
mkdir gitlocalrepo
cd gitlocalrepo/
git clone https://gitlab.epfl.ch/si-exheb/salt-install.git

```
### 5. Launch the script  ###

And... launch the setup-salt-minion-sles11.sh script with the good option:

* -h  for the help on the script
* -t to test the target's OS, to be sure of the version before act.
* -r to install the repo for Salt products
* -s to install the Salt Minion tool on the target

```

cd salt-install/Minions-install/3-Scripts/sles_11/

sudo sh setup-salt-minion-sles11.sh -h

sudo sh setup-salt-minion-sles11.sh -t

sudo sh setup-salt-minion-sles11.sh -r 

sudo sh setup-salt-minion-sles11.sh -s

```

When you launch the "Repo install" or the "Salt-minion" install, the both
scripts want to know if you validate the key for the New settled Repo.

The question is :

```

Retrieving repository 'SaltStack, dependencies, and addons (SLE_11_SP4)' metadata [/]

New repository or package signing key received:
Key ID: 2ABFA143A0E46E11
Key Name: systemsmanagement OBS Project <systemsmanagement@build.opensuse.org>
Key Fingerprint: 68D3387499670AEBD9882DB32ABFA143A0E46E11
Key Created: Sun 15 Mar 2015 12:19:59 PM EDT
Key Expires: Tue 23 May 2017 12:19:59 PM EDT
Repository: SaltStack, dependencies, and addons (SLE_11_SP4)

Do you want to reject the key, trust temporarily, or trust always? [r/t/a/? shows all options] (r):

```

You must to set to a (trust always), otherwise, by default, it reject the key.

If this is all ok, we can catch the target on Salt Master of EXHEB.

Thank you !

Philippe Cotter