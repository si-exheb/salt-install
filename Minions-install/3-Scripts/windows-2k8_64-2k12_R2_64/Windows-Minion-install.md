# Windows Minion Install procedure #

This readme explain the complete procedure for the Windows 2k8 and 2k12 
versions.

# First steps, prepair:

## Go on the target server with RDP

## Download the installer (choose the right version, 32b or 64b): ##

Save it locally (C:\temp\ for example)

### 32 bits: ###

```

https://repo.saltstack.com/windows/Salt-Minion-2015.8.7-x86-Setup.exe

```

### 64 bits: ###

```

https://repo.saltstack.com/windows/Salt-Minion-2015.8.7-AMD64-Setup.exe

```

## Details:

- The 64bit installer has been tested on Windows Server 2008 64bit and Windows Server 2012 R2 64bit. 
- The installer asks for 2 bits of information; the master hostname and the minion name. 
- The installer will update the minion config with these options and then start the minion.
- The salt-minion service will appear in the Windows Service Manager and can be started and stopped there or with the command line program sc like any other Windows service.
- If the minion won't start, try installing the Microsoft Visual C++ 2008 x64 SP1 redistributable. 
- Allow all Windows updates to run salt-minion smoothly.


# Installation of the Minion:

There is 2 ways to install it, manual mode or Silent mode.
I recommand the second one, the Silent mode with one unique command
launched on a Command Prompt (cmd.exe).

## Install of the Minion - MANUAL INSTALL with options to set:

- Launch the setup program for the good version (32 or 64 bits) and with the Administrator rights (right button first).
- The setup program start and click on "Next" button
- Validate the License Agreement by clicking "I Agree" button
- Now you must enter 2 things:
    - The Salt Master IP or Hostname
    - The Minion name (the target server's hostname, FQN, like ditextest.epfl.ch)
    And click on "Install" button
- The setup program is now complete and ask you if you want to launch the service. Select "Run Salt Minion" and on "Finish" button

## OR,...

## Install of the Minion with SILENT INSTALLER OPTIONS:

The installer can be run silently by providing the /S option at the command line. 

The installer also accepts the following options for configuring the Salt Minion silently:

/master= A string value to set the IP address or host name of the master. Default value is 'salt', you must change for the exhebcfg01.epfl.ch

/minion-name= A string value to set the minion name. Default is 'hostname', you must change for the real target's name (FQN)

/start-service= Either a 1 or 0. '1' will start the service, '0' will not. Default is to start the service after installation, so '1'.

Here's an example of using the silent installer:

Use the Command Prompt (cmd.exe)

Go on the download local path (c:\temp\ in the example)


```

Salt-Minion-2015.8.7-AMD64-Setup /S /master=exhebcfg01.epfl.ch /minion-name=Target-Machine-Name /start-service=1

```

Be careful of the version of the .exe, it could change if the Minion's installer is newer.


## Verification:

You must verify that the service is now running in the Windows Task Manager, the services is named salt-minion.
And you must to verify that this service is running wit Startup Type automatic and logon on Local System (open the services
window and check the salt-minion service).

Connect on the Production Salt Master console (ssh) and verify that the Minion is reachable.
To do this, you must use the import of all new keys with:

```

sudo salt-key -y -A

```

After that, you can test the arrival of the new Target's Minion with:

```

sudo salt '*' test.ping

```

It must respond this:

```

[cotter@exhebcfg01 ~]$ sudo salt 'ditextestpco.epfl.ch' test.ping
[sudo] password for cotter:
ditextestpco.epfl.ch:
    True
[cotter@exhebcfg01 ~]$

```


## Uninstall:

### On the Target itself:

- Launch the "Programs and Features" tool in the control panetl
- Select the Salt Minion tool and click on the "Uninstall" button

### On the Master:

When you uninstall a Minion, on the Master, you must remove 
the Target's key with the following command:

```

sudo salt-key -d 'ditextestpco.epfl.ch'

```

And test to list the all Minions with the following command to be sure
that the Target is not anymore included:

```

sudo salt '*' test.ping

```