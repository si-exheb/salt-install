# Setup Documentation for a minion on CentOS 5 OS#

Please use the setup-salt-minion-centos5.sh only on CentOS version 5.

## On the target server: ##

### 1. Connect on the target server ###

```
ssh username@targetserver
```

### 2. Verifiy the sudoing capability. ###

```
sudo su -
```

After credentials returned, if the return prompt is root@targetmachinename, it's 
all ok to continue.

```
exit
```

To go back in normal user mode.

### 2. install git and wget tools ###

Wget is totally non-invasive and have no dependencies. Totally safe to install.

Git is non-invasive, it have only dependencies of pearl libs. Please
check on the target server if it could disturb or not.

```
sudo wget http://packages.sw.be/rpmforge-release/rpmforge-release-0.5.2-2.el5.rf.i386.rpm
sudo rpm --import http://apt.sw.be/RPM-GPG-KEY.dag.txt
sudo rpm -K rpmforge-release-0.5.2-2.el5.rf.i386.rpm
sudo rpm -i rpmforge-release-0.5.2-2.el5.rf.i386.rpm
sudo yum install git-gui

sudo yum install wget
```

### 3. go on the home and git clone the project ###

Use your username and password for gitlab.epfl.ch when you pull the
project.

```
cd $home
mkdir gitlocalrepo
cd gitlocalrepo/
git clone https://gitlab.epfl.ch/si-exheb/salt-install.git

```
### 5. Launch the script  ###


And... launch the setup-salt-minion-centos5.sh script with the good option:

* -h  for the help on the script
* -t to test the target's OS, to be sure that's a CentOS v. 5 before act.
* -s to install the Salt Minion tool on the target

```

cd salt-install/Minions-install/3-Scripts/centos_5

sudo sh setup-salt-minion-centos5.sh -h

sudo sh setup-salt-minion-centos5.sh -t

sudo sh setup-salt-minion-centos5.sh -s

```

If this is all ok, we can catch the target on Salt Master of EXHEB.

Thank you !

Philippe Cotter