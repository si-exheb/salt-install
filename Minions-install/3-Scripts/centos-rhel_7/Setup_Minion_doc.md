# Setup Documentation for a minion on CentOS7 & RHEL 7 OS#

Please use the setup-salt-minion-centos7_rhel7.sh only on CentOS and RHEL version
7.

## On the target server: ##

### 1. Connect on the target server ###

```
ssh username@targetserver
```

### 2. Verifiy the sudoing capability. ###

```
sudo su -
```

After credentials returned, if the return prompt is root@targetmachinename, it's 
all ok to continue.

```
exit
```

To go back in normal user mode.

### 3. install git and wget tools ###

Wget is totally non-invasive and have no dependencies. Totally safe to install.

Git is non-invasive, it have only dependencies of pearl libs. Please
check on the target server if it could disturb or not.

```
sudo yum install git  
sudo yum install wget
```

### 4. go on the username's home and git clone the project ###

Use your username and password for gitlab.epfl.ch when you pull the
project.

```
cd $home
mkdir gitlocalrepo
cd gitlocalrepo/
git clone https://gitlab.epfl.ch/si-exheb/salt-install.git

```
### 5. Launch the script  ###


And... launch the setup-salt-minion-centos7_rhel7.sh script with the good option:

* -h  for the help on the script
* -t to test the target's OS, to be sure that's a CentOS or RHEL v. 7 before act.
* -s to install the Salt Minion tool on the target

```

cd salt-install/Minions-install/3-Scripts/centos-rhel_7

sudo sh setup-salt-minion-centos7_rhel7.sh -h

sudo sh setup-salt-minion-centos7_rhel7.sh -t

sudo sh setup-salt-minion-centos7_rhel7.sh -s

```

If this is all ok, we can catch the target on Salt Master of EXHEB.

Thank you !

Philippe Cotter